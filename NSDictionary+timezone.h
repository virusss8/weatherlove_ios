//
//  NSDictionary+timezone.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeFactory.h"

@interface NSDictionary (timezone)

- (NSString *)timezone_query;
- (NSString *)timezone_type;
- (NSString *)timezone_localTime;
- (NSNumber *)timezone_utcOffset;
/*!!!*/- (NSDate *)timezone_utcTime;

@end
