//
//  CurrentConditions.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentConditions : NSObject

@property (nonatomic, strong) NSNumber *cloudCover;
@property (nonatomic, strong) NSNumber *feelsLikeC;
@property (nonatomic, strong) NSNumber *feelsLikeF;
@property (nonatomic, strong) NSNumber *humidity;
@property (nonatomic, strong) NSNumber *isDaytime;
//@property (nonatomic, strong) NSDate *localObservationDateTime;
//@property (nonatomic, strong) NSDate *observationTime;
@property (nonatomic, strong) NSNumber *precipitation;
@property (nonatomic, strong) NSNumber *pressure;
@property (nonatomic, strong) NSNumber *tempC;
@property (nonatomic, strong) NSNumber *tempF;
@property (nonatomic, strong) NSNumber *visibility;
@property (nonatomic, strong) NSNumber *weatherCode;
@property (nonatomic, strong) NSString *weatherDescription;
//@property (nonatomic, strong) NSURL *weatherIconURL;
@property (nonatomic, strong) NSString *windDirection16Point;
@property (nonatomic, strong) NSNumber *windDirectionDegree;
@property (nonatomic, strong) NSNumber *windSpeedKmph;
@property (nonatomic, strong) NSNumber *windSpeedMiles;

//@property (nonatomic, strong) UIImage *weatherIcon;
//@property (nonatomic, strong) NSNumber *temperature;
//@property (nonatomic, strong) NSString *description;
//@property (nonatomic, strong) NSNumber *windSpeed;
//@property (nonatomic, strong) UIImage *windIcon;
//@property (nonatomic, strong) NSString *sunrise;
//@property (nonatomic, strong) NSString *sunset;
//@property (nonatomic, strong) NSArray *daily;

+ (CurrentConditions *)sharedCurrentConditionsInstance;

@end
