//
//  NSDictionary+current_condition.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (current_condition)

- (NSNumber *)cloudCover_current; // integer
- (NSNumber *)feelsLikeC_current; // integer
- (NSNumber *)feelsLikeF_current; // integer
- (NSNumber *)humidity_current; // integer
- (NSNumber *)isDaytime_current; // boolean
- (NSString *)localObservationDateTime_current; // date format: YYYY-MM-DD HH:mm AM/PM
- (NSString *)observationTime_current; // date format: HH:mm AM/PM
/*!!!*/- (NSDate *)gmtObservationDateTime_current;
- (NSNumber *)precipitation_current; // double
- (NSNumber *)pressure_current; // integer
- (NSNumber *)tempC_current; // integer
- (NSNumber *)tempF_current; // integer
- (NSNumber *)visibility_current; // integer
- (NSNumber *)weatherCode_current; // integer
- (NSString *)weatherDescription_current; // string
- (NSURL *)weatherIconURL_current; // url
- (NSString *)windDirection16Point_current; // string
- (NSNumber *)windDirectionDegree_current; // integer
- (NSNumber *)windSpeedKmph_current; // integer
- (NSNumber *)windSpeedMiles_current; // integer

@end
