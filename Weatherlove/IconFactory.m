//
//  IconFactory.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

static NSString * const DEFAULT_TFS_ICON = @"weather_earth";
static NSString * const DEFAULT_TICK_ICON = @"tick_weather_sunny";
static NSString * const DEFAULT_CLIMA_DARK_ICON = @"clima_dark_weather_sunny";
static NSString * const DEFAULT_CLIMA_LIGHT_ICON = @"clima_light_weather_sunny";
static NSString * const DEFAULT_WIND_ICON = @"compass_n";


#import "IconFactory.h"

@implementation IconFactory

+ (IconFactory *)sharedIconFactoryInstance
{
    static IconFactory *_sharedIconFactoryInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedIconFactoryInstance = [[self alloc] init];
    });
    
    return _sharedIconFactoryInstance;
}

- (UIImage *)getWeatherIconImageFromCode:(NSNumber *)code andWithDayNightContext:(int)context
{
    // TODO: iconImage from code returner
    if (context == ICON_CONTEXT_DAILY) {
        return [self getDailyIconFromCode:[code intValue]];
    } else if (context == ICON_CONTEXT_NIGHTLY) {
        return [self getNighlyIconFromCode:[code intValue]];
    } else {
        return nil;
    }
}

- (UIImage *)getWindIconImage:(NSString *)key
{
    if ([key isEqualToString:@"N"]) {
        return [UIImage imageNamed:@"compass_s"];
        
    } else if ([key isEqualToString:@"NNE"]) {
        return [UIImage imageNamed:@"compass_ssw"];
        
    } else if ([key isEqualToString:@"NE"]) {
        return [UIImage imageNamed:@"compass_sw"];
        
    } else if ([key isEqualToString:@"ENE"]) {
        return [UIImage imageNamed:@"compass_wsw"];
        
    } else if ([key isEqualToString:@"E"]) {
        return [UIImage imageNamed:@"compass_w"];
        
    } else if ([key isEqualToString:@"ESE"]) {
        return [UIImage imageNamed:@"compass_wnw"];
        
    } else if ([key isEqualToString:@"SE"]) {
        return [UIImage imageNamed:@"compass_nw"];
        
    } else if ([key isEqualToString:@"SSE"]) {
        return [UIImage imageNamed:@"compass_nnw"];
        
    } else if ([key isEqualToString:@"S"]) {
        return [UIImage imageNamed:@"compass_n"];
        
    } else if ([key isEqualToString:@"SSW"]) {
        return [UIImage imageNamed:@"compass_nne"];
        
    } else if ([key isEqualToString:@"SW"]) {
        return [UIImage imageNamed:@"compass_ne"];
        
    } else if ([key isEqualToString:@"WSW"]) {
        return [UIImage imageNamed:@"compass_ene"];
        
    } else if ([key isEqualToString:@"W"]) {
        return [UIImage imageNamed:@"compass_e"];
        
    } else if ([key isEqualToString:@"WNW"]) {
        return [UIImage imageNamed:@"compass_ese"];
        
    } else if ([key isEqualToString:@"NW"]) {
        return [UIImage imageNamed:@"compass_se"];
        
    } else if ([key isEqualToString:@"NNW"]) {
        return [UIImage imageNamed:@"compass_sse"];
        
    } else {
        return [UIImage imageNamed:DEFAULT_WIND_ICON];
    }
}

- (UIImage *)getNighlyIconFromCode:(int)code
{
    switch ([[Util getSharedPrefs:_ICON_TYPE_VAR_NAME] intValue]) {
        case ICON_TYPE_TFS_FLATTIES:
        return [self getIcon_TFS_Flatties_Nightly:code];
        
        case ICON_TYPE_CLIMA_DARK:
        return [self getIcon_Clima_Dark_Nightly:code];
        
        case ICON_TYPE_CLIMA_LIGHT:
        return [self getIcon_Clima_Light_Nightly:code];
        
        case ICON_TYPE_TICK:
        return [self getIcon_Tick_Nightly:code];
        
        case ICON_TYPE_TFS_ORIGINAL:
        default:
        return [self getIcon_TFS_Nightly:code];
    }
}

- (UIImage *)getDailyIconFromCode:(int)code
{
    switch ([[Util getSharedPrefs:_ICON_TYPE_VAR_NAME] intValue]) {
        case ICON_TYPE_TFS_FLATTIES:
        return [self getIcon_TFS_Flatties_Daily:code];
        
        case ICON_TYPE_CLIMA_DARK:
        return [self getIcon_Clima_Dark_Daily:code];
        
        case ICON_TYPE_CLIMA_LIGHT:
        return [self getIcon_Clima_Light_Daily:code];
        
        case ICON_TYPE_TICK:
        return [self getIcon_Tick_Daily:code];
        
        case ICON_TYPE_TFS_ORIGINAL:
        default:
        return [self getIcon_TFS_Daily:code];
    }
}

- (UIImage *)getIcon_TFS_Daily:(int)code {
    switch (code) {
        case 395:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 392:
        return [UIImage imageNamed:@"weather_chanceflurries"];
        
        case 389:
        return [UIImage imageNamed:@"weather_tstorms"];
        
        case 386:
        return [UIImage imageNamed:@"weather_chancetstorms"];
        
        case 377:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 374:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 371:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 368:
        return [UIImage imageNamed:@"weather_chancesnow"];
        
        case 365:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 362:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 359:
        return [UIImage imageNamed:@"weather_rain"];
        
        case 356:
        return [UIImage imageNamed:@"weather_rain"];
        
        case 353:
        return [UIImage imageNamed:@"weather_lightrain"];
        
        case 350:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 338:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 335:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 332:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 329:
        return [UIImage imageNamed:@"weather_chancesnow"];
        
        case 326:
        return [UIImage imageNamed:@"weather_flurries"];
        
        case 323:
        return [UIImage imageNamed:@"weather_chanceflurries"];
        
        case 320:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 317:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 314:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 311:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 308:
        return [UIImage imageNamed:@"weather_rain"];
        
        case 305:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 302:
        return [UIImage imageNamed:@"weather_rain"];
        
        case 299:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 296:
        return [UIImage imageNamed:@"weather_lightrain"];
        
        case 293:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 284:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 281:
        return [UIImage imageNamed:@"weather_sleet"];
        
        case 266:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 263:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 260:
        return [UIImage imageNamed:@"weather_fog"];
        
        case 248:
        return [UIImage imageNamed:@"weather_fog"];
        
        case 230:
        return [UIImage imageNamed:@"weather_snow"];
        
        case 227:
        return [UIImage imageNamed:@"weather_chancesnow"];
        
        case 200:
        return [UIImage imageNamed:@"weather_chancetstorms"];
        
        case 185:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 182:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 179:
        return [UIImage imageNamed:@"weather_chancesleet"];
        
        case 176:
        return [UIImage imageNamed:@"weather_chancerain"];
        
        case 143:
        return [UIImage imageNamed:@"weather_fog"];
        
        case 122:
        return [UIImage imageNamed:@"weather_cloudy"];
        
        case 119:
        return [UIImage imageNamed:@"weather_cloudy"];
        
        case 116:
        return [UIImage imageNamed:@"weather_mostlysunny"];
        
        case 113:
        return [UIImage imageNamed:@"weather_sunny"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TFS_ICON];
    }
}

- (UIImage *)getIcon_TFS_Nightly:(int)code {
    switch (code) {
        case 392:
        return [UIImage imageNamed:@"weather_chanceflurries_n"];
        
        case 386:
        return [UIImage imageNamed:@"weather_chancetstorms_n"];
        
        case 374:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 368:
        return [UIImage imageNamed:@"weather_chancesnow_n"];
        
        case 362:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 329:
        return [UIImage imageNamed:@"weather_chancesnow_n"];
        
        case 323:
        return [UIImage imageNamed:@"weather_chanceflurries_n"];
        
        case 317:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 311:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 305:
        return [UIImage imageNamed:@"weather_chancerain_n"];
        
        case 299:
        return [UIImage imageNamed:@"weather_chancerain_n"];
        
        case 293:
        return [UIImage imageNamed:@"weather_chancerain_n"];
        
        case 266:
        return [UIImage imageNamed:@"weather_chancerain_n"];
        
        case 263:
        return [UIImage imageNamed:@"weather_chancerain_n"];
        
        case 227:
        return [UIImage imageNamed:@"weather_chancesnow_n"];
        
        case 200:
        return [UIImage imageNamed:@"weather_chancetstorms_n"];
        
        case 185:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 182:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 179:
        return [UIImage imageNamed:@"weather_chancesleet_n"];
        
        case 176:
        return [UIImage imageNamed:@"weather_chancerain_n"];

        case 116:
        return [UIImage imageNamed:@"weather_mostlysunny_n"];
        
        case 113:
        return [UIImage imageNamed:@"weather_sunny_n"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TFS_ICON];
    }
}

- (UIImage *)getIcon_TFS_Flatties_Daily:(int)code {
    switch (code) {
        case 395:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 392:
        return [UIImage imageNamed:@"weatherflat_chanceflurries"];
        
        case 389:
        return [UIImage imageNamed:@"weatherflat_tstorms"];
        
        case 386:
        return [UIImage imageNamed:@"weatherflat_chancetstorms"];
        
        case 377:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 374:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 371:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 368:
        return [UIImage imageNamed:@"weatherflat_chancesnow"];
        
        case 365:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 362:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 359:
        return [UIImage imageNamed:@"weatherflat_rain"];
        
        case 356:
        return [UIImage imageNamed:@"weatherflat_rain"];
        
        case 353:
        return [UIImage imageNamed:@"weatherflat_lightrain"];
        
        case 350:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 338:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 335:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 332:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 329:
        return [UIImage imageNamed:@"weatherflat_chancesnow"];
        
        case 326:
        return [UIImage imageNamed:@"weatherflat_flurries"];
        
        case 323:
        return [UIImage imageNamed:@"weatherflat_chanceflurries"];
        
        case 320:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 317:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 314:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 311:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 308:
        return [UIImage imageNamed:@"weatherflat_rain"];
        
        case 305:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 302:
        return [UIImage imageNamed:@"weatherflat_rain"];
        
        case 299:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 296:
        return [UIImage imageNamed:@"weatherflat_lightrain"];
        
        case 293:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 284:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 281:
        return [UIImage imageNamed:@"weatherflat_sleet"];
        
        case 266:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 263:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 260:
        return [UIImage imageNamed:@"weatherflat_fog"];
        
        case 248:
        return [UIImage imageNamed:@"weatherflat_fog"];
        
        case 230:
        return [UIImage imageNamed:@"weatherflat_snow"];
        
        case 227:
        return [UIImage imageNamed:@"weatherflat_chancesnow"];
        
        case 200:
        return [UIImage imageNamed:@"weatherflat_chancetstorms"];
        
        case 185:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 182:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 179:
        return [UIImage imageNamed:@"weatherflat_chancesleet"];
        
        case 176:
        return [UIImage imageNamed:@"weatherflat_chancerain"];
        
        case 143:
        return [UIImage imageNamed:@"weatherflat_fog"];
        
        case 122:
        return [UIImage imageNamed:@"weatherflat_cloudy"];
        
        case 119:
        return [UIImage imageNamed:@"weatherflat_cloudy"];
        
        case 116:
        return [UIImage imageNamed:@"weatherflat_mostlysunny"];
        
        case 113:
        return [UIImage imageNamed:@"weatherflat_sunny"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TFS_ICON];
    }
}

- (UIImage *)getIcon_TFS_Flatties_Nightly:(int)code {
    switch (code) {
        case 392:
        return [UIImage imageNamed:@"weatherflat_chanceflurries_n"];
        
        case 386:
        return [UIImage imageNamed:@"weatherflat_chancetstorms_n"];
        
        case 374:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 368:
        return [UIImage imageNamed:@"weatherflat_chancesnow_n"];
        
        case 362:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 329:
        return [UIImage imageNamed:@"weatherflat_chancesnow_n"];
        
        case 323:
        return [UIImage imageNamed:@"weatherflat_chanceflurries_n"];
        
        case 317:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 311:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 305:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 299:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 293:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 266:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 263:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 227:
        return [UIImage imageNamed:@"weatherflat_chancesnow_n"];
        
        case 200:
        return [UIImage imageNamed:@"weatherflat_chancetstorms_n"];
        
        case 185:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 182:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 179:
        return [UIImage imageNamed:@"weatherflat_chancesleet_n"];
        
        case 176:
        return [UIImage imageNamed:@"weatherflat_chancerain_n"];
        
        case 116:
        return [UIImage imageNamed:@"weatherflat_mostlysunny_n"];
        
        case 113:
        return [UIImage imageNamed:@"weatherflat_sunny_n"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TFS_ICON];
    }
}

- (UIImage *)getIcon_Tick_Daily:(int)code {
    switch (code) {
        case 395:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 392:
        return [UIImage imageNamed:@"tick_weather_chanceflurries"];
        
        case 389:
        return [UIImage imageNamed:@"tick_weather_tstorms"];
        
        case 386:
        return [UIImage imageNamed:@"tick_weather_chancetstorms"];
        
        case 377:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 374:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 371:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 368:
        return [UIImage imageNamed:@"tick_weather_chancesnow"];
        
        case 365:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 362:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 359:
        return [UIImage imageNamed:@"tick_weather_rain"];
        
        case 356:
        return [UIImage imageNamed:@"tick_weather_rain"];
        
        case 353:
        return [UIImage imageNamed:@"tick_weather_lightrain"];
        
        case 350:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 338:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 335:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 332:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 329:
        return [UIImage imageNamed:@"tick_weather_chancesnow"];
        
        case 326:
        return [UIImage imageNamed:@"tick_weather_flurries"];
        
        case 323:
        return [UIImage imageNamed:@"tick_weather_chanceflurries"];
        
        case 320:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 317:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 314:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 311:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 308:
        return [UIImage imageNamed:@"tick_weather_rain"];
        
        case 305:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 302:
        return [UIImage imageNamed:@"tick_weather_rain"];
        
        case 299:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 296:
        return [UIImage imageNamed:@"tick_weather_lightrain"];
        
        case 293:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 284:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 281:
        return [UIImage imageNamed:@"tick_weather_sleet"];
        
        case 266:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 263:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 260:
        return [UIImage imageNamed:@"tick_weather_fog"];
        
        case 248:
        return [UIImage imageNamed:@"tick_weather_fog"];
        
        case 230:
        return [UIImage imageNamed:@"tick_weather_snow"];
        
        case 227:
        return [UIImage imageNamed:@"tick_weather_chancesnow"];
        
        case 200:
        return [UIImage imageNamed:@"tick_weather_chancetstorms"];
        
        case 185:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 182:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 179:
        return [UIImage imageNamed:@"tick_weather_chancesleet"];
        
        case 176:
        return [UIImage imageNamed:@"tick_weather_chancerain"];
        
        case 143:
        return [UIImage imageNamed:@"tick_weather_fog"];
        
        case 122:
        return [UIImage imageNamed:@"tick_weather_cloudy"];
        
        case 119:
        return [UIImage imageNamed:@"tick_weather_cloudy"];
        
        case 116:
        return [UIImage imageNamed:@"tick_weather_mostlysunny"];
        
        case 113:
        return [UIImage imageNamed:@"tick_weather_sunny"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TICK_ICON];
    }
}

- (UIImage *)getIcon_Tick_Nightly:(int)code {
    switch (code) {
        case 392:
        return [UIImage imageNamed:@"tick_weather_chanceflurries_n"];
        
        case 386:
        return [UIImage imageNamed:@"tick_weather_chancetstorms_n"];
        
        case 374:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 368:
        return [UIImage imageNamed:@"tick_weather_chancesnow_n"];
        
        case 362:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 329:
        return [UIImage imageNamed:@"tick_weather_chancesnow_n"];
        
        case 323:
        return [UIImage imageNamed:@"tick_weather_chanceflurries_n"];
        
        case 317:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 311:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 305:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 299:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 293:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 266:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 263:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 227:
        return [UIImage imageNamed:@"tick_weather_chancesnow_n"];
        
        case 200:
        return [UIImage imageNamed:@"tick_weather_chancetstorms_n"];
        
        case 185:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 182:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 179:
        return [UIImage imageNamed:@"tick_weather_chancesleet_n"];
        
        case 176:
        return [UIImage imageNamed:@"tick_weather_chancerain_n"];
        
        case 116:
        return [UIImage imageNamed:@"tick_weather_mostlysunny_n"];
        
        case 113:
        return [UIImage imageNamed:@"tick_weather_sunny_n"];
        
        default:
        return [UIImage imageNamed:DEFAULT_TICK_ICON];
    }
}

- (UIImage *)getIcon_Clima_Dark_Daily:(int)code {
    switch (code) {
        case 395:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 392:
        return [UIImage imageNamed:@"clima_dark_weather_chanceflurries"];
        
        case 389:
        return [UIImage imageNamed:@"clima_dark_weather_tstorms"];
        
        case 386:
        return [UIImage imageNamed:@"clima_dark_weather_chancetstorms"];
        
        case 377:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 374:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 371:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 368:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow"];
        
        case 365:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 362:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 359:
        return [UIImage imageNamed:@"clima_dark_weather_rain"];
        
        case 356:
        return [UIImage imageNamed:@"clima_dark_weather_rain"];
        
        case 353:
        return [UIImage imageNamed:@"clima_dark_weather_lightrain"];
        
        case 350:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 338:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 335:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 332:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 329:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow"];
        
        case 326:
        return [UIImage imageNamed:@"clima_dark_weather_flurries"];
        
        case 323:
        return [UIImage imageNamed:@"clima_dark_weather_chanceflurries"];
        
        case 320:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 317:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 314:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 311:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 308:
        return [UIImage imageNamed:@"clima_dark_weather_rain"];
        
        case 305:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 302:
        return [UIImage imageNamed:@"clima_dark_weather_rain"];
        
        case 299:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 296:
        return [UIImage imageNamed:@"clima_dark_weather_lightrain"];
        
        case 293:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 284:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 281:
        return [UIImage imageNamed:@"clima_dark_weather_sleet"];
        
        case 266:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 263:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 260:
        return [UIImage imageNamed:@"clima_dark_weather_fog"];
        
        case 248:
        return [UIImage imageNamed:@"clima_dark_weather_fog"];
        
        case 230:
        return [UIImage imageNamed:@"clima_dark_weather_snow"];
        
        case 227:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow"];
        
        case 200:
        return [UIImage imageNamed:@"clima_dark_weather_chancetstorms"];
        
        case 185:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 182:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 179:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet"];
        
        case 176:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain"];
        
        case 143:
        return [UIImage imageNamed:@"clima_dark_weather_fog"];
        
        case 122:
        return [UIImage imageNamed:@"clima_dark_weather_cloudy"];
        
        case 119:
        return [UIImage imageNamed:@"clima_dark_weather_cloudy"];
        
        case 116:
        return [UIImage imageNamed:@"clima_dark_weather_mostlysunny"];
        
        case 113:
        return [UIImage imageNamed:@"clima_dark_weather_sunny"];
        
        default:
        return [UIImage imageNamed:DEFAULT_CLIMA_DARK_ICON];
    }
}

- (UIImage *)getIcon_Clima_Dark_Nightly:(int)code {
    switch (code) {
        case 392:
        return [UIImage imageNamed:@"clima_dark_weather_chanceflurries_n"];
        
        case 386:
        return [UIImage imageNamed:@"clima_dark_weather_chancetstorms_n"];
        
        case 374:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 368:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow_n"];
        
        case 362:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 329:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow_n"];
        
        case 323:
        return [UIImage imageNamed:@"clima_dark_weather_chanceflurries_n"];
        
        case 317:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 311:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 305:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 299:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 293:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 266:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 263:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 227:
        return [UIImage imageNamed:@"clima_dark_weather_chancesnow_n"];
        
        case 200:
        return [UIImage imageNamed:@"clima_dark_weather_chancetstorms_n"];
        
        case 185:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 182:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 179:
        return [UIImage imageNamed:@"clima_dark_weather_chancesleet_n"];
        
        case 176:
        return [UIImage imageNamed:@"clima_dark_weather_chancerain_n"];
        
        case 116:
        return [UIImage imageNamed:@"clima_dark_weather_mostlysunny_n"];
        
        case 113:
        return [UIImage imageNamed:@"clima_dark_weather_sunny_n"];
        
        default:
        return [UIImage imageNamed:DEFAULT_CLIMA_DARK_ICON];
    }
}

- (UIImage *)getIcon_Clima_Light_Daily:(int)code {
    switch (code) {
        case 395:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 392:
        return [UIImage imageNamed:@"clima_light_weather_chanceflurries"];
        
        case 389:
        return [UIImage imageNamed:@"clima_light_weather_tstorms"];
        
        case 386:
        return [UIImage imageNamed:@"clima_light_weather_chancetstorms"];
        
        case 377:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 374:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 371:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 368:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow"];
        
        case 365:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 362:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 359:
        return [UIImage imageNamed:@"clima_light_weather_rain"];
        
        case 356:
        return [UIImage imageNamed:@"clima_light_weather_rain"];
        
        case 353:
        return [UIImage imageNamed:@"clima_light_weather_lightrain"];
        
        case 350:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 338:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 335:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 332:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 329:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow"];
        
        case 326:
        return [UIImage imageNamed:@"clima_light_weather_flurries"];
        
        case 323:
        return [UIImage imageNamed:@"clima_light_weather_chanceflurries"];
        
        case 320:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 317:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 314:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 311:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 308:
        return [UIImage imageNamed:@"clima_light_weather_rain"];
        
        case 305:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 302:
        return [UIImage imageNamed:@"clima_light_weather_rain"];
        
        case 299:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 296:
        return [UIImage imageNamed:@"clima_light_weather_lightrain"];
        
        case 293:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 284:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 281:
        return [UIImage imageNamed:@"clima_light_weather_sleet"];
        
        case 266:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 263:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 260:
        return [UIImage imageNamed:@"clima_light_weather_fog"];
        
        case 248:
        return [UIImage imageNamed:@"clima_light_weather_fog"];
        
        case 230:
        return [UIImage imageNamed:@"clima_light_weather_snow"];
        
        case 227:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow"];
        
        case 200:
        return [UIImage imageNamed:@"clima_light_weather_chancetstorms"];
        
        case 185:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 182:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 179:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet"];
        
        case 176:
        return [UIImage imageNamed:@"clima_light_weather_chancerain"];
        
        case 143:
        return [UIImage imageNamed:@"clima_light_weather_fog"];
        
        case 122:
        return [UIImage imageNamed:@"clima_light_weather_cloudy"];
        
        case 119:
        return [UIImage imageNamed:@"clima_light_weather_cloudy"];
        
        case 116:
        return [UIImage imageNamed:@"clima_light_weather_mostlysunny"];
        
        case 113:
        return [UIImage imageNamed:@"clima_light_weather_sunny"];
        
        default:
        return [UIImage imageNamed:DEFAULT_CLIMA_LIGHT_ICON];
    }
}

- (UIImage *)getIcon_Clima_Light_Nightly:(int)code {
    switch (code) {
        case 392:
        return [UIImage imageNamed:@"clima_light_weather_chanceflurries_n"];
        
        case 386:
        return [UIImage imageNamed:@"clima_light_weather_chancetstorms_n"];
        
        case 374:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 368:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow_n"];
        
        case 362:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 329:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow_n"];
        
        case 323:
        return [UIImage imageNamed:@"clima_light_weather_chanceflurries_n"];
        
        case 317:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 311:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 305:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 299:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 293:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 266:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 263:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 227:
        return [UIImage imageNamed:@"clima_light_weather_chancesnow_n"];
        
        case 200:
        return [UIImage imageNamed:@"clima_light_weather_chancetstorms_n"];
        
        case 185:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 182:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 179:
        return [UIImage imageNamed:@"clima_light_weather_chancesleet_n"];
        
        case 176:
        return [UIImage imageNamed:@"clima_light_weather_chancerain_n"];
        
        case 116:
        return [UIImage imageNamed:@"clima_light_weather_mostlysunny_n"];
        
        case 113:
        return [UIImage imageNamed:@"clima_light_weather_sunny_n"];
        
        default:
        return [UIImage imageNamed:DEFAULT_CLIMA_LIGHT_ICON];
    }
}

@end
