//
//  WeatherForecastHourlyHTTPClient.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol WeatherForecastHourlyHTTPClientDelegate;

@interface WeatherForecastHourlyHTTPClient : AFHTTPSessionManager

@property (nonatomic, weak) id<WeatherForecastHourlyHTTPClientDelegate>delegate;

+ (WeatherForecastHourlyHTTPClient *)sharedWeatherForecastHourlyHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)updateWeatherForecastAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString;

@end

@protocol WeatherForecastHourlyHTTPClientDelegate <NSObject>
@optional
-(void)weatherForecastHourlyHTTPClient:(WeatherForecastHourlyHTTPClient *)client didUpdateWithForecast:(id)weatherHourly onOriginalLocation:(id)location;
-(void)weatherForecastHourlyHTTPClient:(WeatherForecastHourlyHTTPClient *)client didFailWithError:(NSError *)error;
@end
