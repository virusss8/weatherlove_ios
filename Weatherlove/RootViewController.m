//
//  RootViewController.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 02/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
//    DailyForecastViewController *dailyForecastViewController = [self viewControllerAtIndex:0];
    CurrentConditionsViewController *currentConditionsViewController = [self viewControllerAtIndex:1];
//    HourlyForecastViewController *hourlyForecastViewController = [self viewControllerAtIndex:2];
    
    NSArray *viewControllers = @[/*dailyForecastViewController, */currentConditionsViewController/*, hourlyForecastViewController*/];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height -2);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    [self.pageIndicator setNumberOfPages:3];
    [self.pageIndicator setIndicatorMargin:10.0f];
	[self.pageIndicator setIndicatorDiameter:100.0f];
    [self.pageIndicator setPageIndicatorTintColor:[UIColor darkGrayColor]];
    [self.pageIndicator setCurrentPageIndicatorTintColor:[UIColor blueColor]];
    [self.pageIndicator setCurrentPage:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)viewControllerAtIndex:(NSUInteger)index
{
    switch(index) {
        case 0:
        {
            DailyForecastViewController *dailyForecastViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"daily_forecast"];
            return dailyForecastViewController;
        }
            break;
        
        case 1:
        {
            CurrentConditionsViewController *currentConditionsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"current_conditions"];
            return currentConditionsViewController;
        }
            break;
            
        case 2:
        {
            HourlyForecastViewController *hourlyForecastViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"hourly_forecast"];
            return hourlyForecastViewController;
        }
            break;
            
        default:
        {
            return nil;
        }
            break;
    }
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index;
    if ([viewController isKindOfClass:[DailyForecastViewController class]]) {
        index = 0;
    } else if ([viewController isKindOfClass:[CurrentConditionsViewController class]]) {
        index = 1;
    } else {
        index = 2;
    }
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index;
    if ([viewController isKindOfClass:[DailyForecastViewController class]]) {
        index = 0;
    } else if ([viewController isKindOfClass:[CurrentConditionsViewController class]]) {
        index = 1;
    } else {
        index = 2;
    }
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if ((index == 3) || (index == NSNotFound)) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

@end
