//
//  RootViewController.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 02/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SMPageControl.h"
#import "CurrentConditionsViewController.h"
#import "DailyForecastViewController.h"
#import "HourlyForecastViewController.h"

@interface RootViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) IBOutlet SMPageControl *pageIndicator;
@property (strong, nonatomic) UIPageViewController *pageViewController;

@end
