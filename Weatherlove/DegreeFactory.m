//
//  DegreeFactory.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "DegreeFactory.h"

@implementation DegreeFactory

+ (DegreeFactory *)sharedDegreeFactoryInstance
{
    static DegreeFactory *_sharedDegreeFactoryInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDegreeFactoryInstance = [[self alloc] init];
    });
    
    return _sharedDegreeFactoryInstance;
}

- (NSNumber *)getFormattedDegreeInCelsius:(NSNumber *)tempC orFahrenheit:(NSNumber *)tempF
{
//    NSLog(@"getFormattedDegreeInCelsius_tempC = %i", [tempC intValue]);
//    NSLog(@"getFormattedDegreeInCelsius_tempF = %i", [tempF intValue]);
    if (tempC != nil && tempF != nil) {
        if ([[Util getSharedPrefs:_DEGREE_FORMAT_VAR_NAME] intValue] == DEGREE_FORMAT_CELSIUS) {
            return tempC;
        } else if ([[Util getSharedPrefs:_DEGREE_FORMAT_VAR_NAME] intValue] == DEGREE_FORMAT_FAHRENHEIT) {
            return tempF;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

@end
