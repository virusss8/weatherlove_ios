//
//  AppDelegate.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 18/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
