//
//  MetricFactory.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "MetricFactory.h"

@implementation MetricFactory

+ (MetricFactory *)sharedMetricFactoryInstance
{
    static MetricFactory *_sharedMetricFactoryInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedMetricFactoryInstance = [[self alloc] init];
    });
    
    return _sharedMetricFactoryInstance;
}

- (NSNumber *)getFormattedMetricsInKmph:(NSNumber *)kmph orMph:(NSNumber *)mph
{
// TODO: formattedMetricsReturner
    if (kmph != nil && mph != nil) {
        if ([[Util getSharedPrefs:_METRIC_FORMAT_VAR_NAME] intValue] == METRIC_FORMAT_KM) {
            return kmph;
        } else if ([[Util getSharedPrefs:_METRIC_FORMAT_VAR_NAME] intValue] == METRIC_FORMAT_MILE) {
            return mph;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

@end
