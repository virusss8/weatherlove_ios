//
//  ViewController.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 18/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherForecastDailyHTTPClient.h"
#import "WeatherForecastHourlyHTTPClient.h"
#import "TimeZoneDataHTTPClient.h"
#import "WeatherReportQueryFactory.h"

@interface ViewController : UIViewController <CLLocationManagerDelegate, WeatherForecastDailyHTTPClientDelegate, WeatherForecastHourlyHTTPClientDelegate, TimeZoneDataHTTPClientDelegate, WeatherReportDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSDictionary *weatherForecastDaily;
@property (nonatomic, strong) NSDictionary *weatherForecastHourly;
@property (nonatomic, strong) NSDictionary *timezoneData;

@property (strong, nonatomic) IBOutlet UIImageView *weatherIcon;
@property (strong, nonatomic) IBOutlet UIImageView *weatherIconDay0;
@property (strong, nonatomic) IBOutlet UIImageView *weatherIconDay1;
@property (strong, nonatomic) IBOutlet UIImageView *weatherIconDay2;
@property (strong, nonatomic) IBOutlet UIImageView *weatherIconDay3;
@property (strong, nonatomic) IBOutlet UIImageView *weatherIconDay4;
@property (strong, nonatomic) IBOutlet UIImageView *windIcon;
@property (strong, nonatomic) IBOutlet UILabel *labelLoc;
- (IBAction)clickHourly:(id)sender;
- (IBAction)clickDaily:(id)sender;
- (IBAction)clickTimezone:(id)sender;
- (IBAction)clickQUERYcurrent:(id)sender;
- (IBAction)clickQUERYdaily:(id)sender;
- (IBAction)clickQUERYhourly:(id)sender;

@property (assign, nonatomic) BOOL isWeatherDailySuccessfullyDownloaded;
@property (assign, nonatomic) BOOL isWeatherHourlySuccessfullyDownloaded;
@property (assign, nonatomic) BOOL isWeatherTimezoneSuccessfullyDownloaded;

@end
