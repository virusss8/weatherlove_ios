//
//  WeatherReportQueryFactory.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 05/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeFactory.h"
#import "MetricFactory.h"
#import "IconFactory.h"
#import "DegreeFactory.h"

@protocol WeatherReportDelegate <NSObject>

- (void)currentWeatherReportCallback:(CurrentConditions *)cc;
- (void)dailyForecastReportCallback:(NSArray *)df;
- (void)hourlyForecastReportCallback:(NSArray *)hf;

@end

@interface WeatherReportQueryFactory : NSObject

@property (nonatomic, weak) id<WeatherReportDelegate> weatherReportDelegate;
@property (nonatomic, strong) NSDictionary *savedDailyForecast;
@property (nonatomic, strong) NSDictionary *savedHourlyForecast;
@property (nonatomic, strong) NSDictionary *savedTimezone;
@property (nonatomic, strong) NSNumber *day;
@property (nonatomic, strong) NSNumber *daysAvailable;

+ (WeatherReportQueryFactory *)sharedWeatherReportQueryFactoryInstance;
//- (NSNumber *)getRightForecastDay;
//- (NSNumber *)getAvailableForecastDay;
- (/*CurrentConditions **/void)getCurrentWeatherReport;
- (/*NSArray **/void)getHourlyForecast;
- (/*NSArray **/void)getDailyForecast;

@end
