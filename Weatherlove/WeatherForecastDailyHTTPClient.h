//
//  WeatherForecastDailyHTTPClient.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol WeatherForecastDailyHTTPClientDelegate;

@interface WeatherForecastDailyHTTPClient : AFHTTPSessionManager

@property (nonatomic, weak) id<WeatherForecastDailyHTTPClientDelegate>delegate;

+ (WeatherForecastDailyHTTPClient *)sharedWeatherForecastDailyHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)updateWeatherForecastAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString;

@end

@protocol WeatherForecastDailyHTTPClientDelegate <NSObject>
@optional
-(void)weatherForecastDailyHTTPClient:(WeatherForecastDailyHTTPClient *)client didUpdateWithForecast:(id)weatherDaily onOriginalLocation:(id)location;
-(void)weatherForecastDailyHTTPClient:(WeatherForecastDailyHTTPClient *)client didFailWithError:(NSError *)error;
@end
