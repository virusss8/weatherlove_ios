//
//  WeatherForecastDailyHTTPClient.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "WeatherForecastDailyHTTPClient.h"

static NSString * const WorldWeatherOnlineAPIKey = @"2rr249nrxq5ykc6ddhm59kyy";

static NSString * const WorldWeatherOnlineURLString = @"http://api.worldweatheronline.com/premium/v1/";

@implementation WeatherForecastDailyHTTPClient

+ (WeatherForecastDailyHTTPClient *)sharedWeatherForecastDailyHTTPClient
{
    static WeatherForecastDailyHTTPClient *_sharedWeatherForecastDailyHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherForecastDailyHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:WorldWeatherOnlineURLString]];
    });
    
    return _sharedWeatherForecastDailyHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)updateWeatherForecastAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    if (locationLatLng != nil && locationString == nil) {
        parameters[@"q"] = [NSString stringWithFormat:@"%f,%f",locationLatLng.coordinate.latitude,locationLatLng.coordinate.longitude];
    } else if (locationString != nil && locationLatLng == nil) {
        parameters[@"q"] = locationString;
    }
    parameters[@"format"] = @"json";
    parameters[@"extra"] = @"localObsTime,isDayTime,utcDateTime";
    parameters[@"num_of_days"] = @(15);
    parameters[@"tp"] = @(24);
    parameters[@"key"] = WorldWeatherOnlineAPIKey;
    
    [self GET:@"weather.ashx" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(weatherForecastDailyHTTPClient:didUpdateWithForecast:onOriginalLocation:)]) {
            if (locationLatLng != nil && locationString == nil) {
                [self.delegate weatherForecastDailyHTTPClient:self didUpdateWithForecast:responseObject onOriginalLocation:locationLatLng];
            } else if (locationString != nil && locationLatLng == nil) {
                [self.delegate weatherForecastDailyHTTPClient:self didUpdateWithForecast:responseObject onOriginalLocation:locationString];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(weatherForecastDailyHTTPClient:didFailWithError:)]) {
            [self.delegate weatherForecastDailyHTTPClient:self didFailWithError:error];
        }
    }];
}

@end
