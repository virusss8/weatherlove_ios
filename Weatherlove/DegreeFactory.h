//
//  DegreeFactory.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DegreeFactory : NSObject

+ (DegreeFactory *)sharedDegreeFactoryInstance;
- (NSNumber *)getFormattedDegreeInCelsius:(NSNumber *)tempC orFahrenheit:(NSNumber *)tempF;

@end
