//
//  IconFactory.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IconFactory : NSObject

+ (IconFactory *)sharedIconFactoryInstance;
- (UIImage *)getWeatherIconImageFromCode:(NSNumber *)code andWithDayNightContext:(int)context;
- (UIImage *)getWindIconImage:(NSString *)key;

@end
