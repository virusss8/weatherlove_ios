//
//  WeatherForecastHourlyHTTPClient.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "WeatherForecastHourlyHTTPClient.h"

static NSString * const WorldWeatherOnlineAPIKey = @"2rr249nrxq5ykc6ddhm59kyy";

static NSString * const WorldWeatherOnlineURLString = @"http://api.worldweatheronline.com/premium/v1/";

@implementation WeatherForecastHourlyHTTPClient

+ (WeatherForecastHourlyHTTPClient *)sharedWeatherForecastHourlyHTTPClient
{
    static WeatherForecastHourlyHTTPClient *_sharedWeatherForecastHourlyHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherForecastHourlyHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:WorldWeatherOnlineURLString]];
    });
    
    return _sharedWeatherForecastHourlyHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)updateWeatherForecastAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    if (locationLatLng != nil && locationString == nil) {
        parameters[@"q"] = [NSString stringWithFormat:@"%f,%f",locationLatLng.coordinate.latitude,locationLatLng.coordinate.longitude];
    } else if (locationString != nil && locationLatLng == nil) {
        parameters[@"q"] = locationString;
    }
    parameters[@"format"] = @"json";
    parameters[@"extra"] = @"localObsTime,isDayTime,utcDateTime";
    parameters[@"num_of_days"] = @(15);
    parameters[@"tp"] = @(3);
    parameters[@"key"] = WorldWeatherOnlineAPIKey;
    
    [self GET:@"weather.ashx" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(weatherForecastHourlyHTTPClient:didUpdateWithForecast:onOriginalLocation:)]) {
            if (locationLatLng != nil && locationString == nil) {
                [self.delegate weatherForecastHourlyHTTPClient:self didUpdateWithForecast:responseObject onOriginalLocation:locationLatLng];
            } else if (locationString != nil && locationLatLng == nil) {
                [self.delegate weatherForecastHourlyHTTPClient:self didUpdateWithForecast:responseObject onOriginalLocation:locationString];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(weatherForecastHourlyHTTPClient:didFailWithError:)]) {
            [self.delegate weatherForecastHourlyHTTPClient:self didFailWithError:error];
        }
    }];
}

@end
