//
//  TimeZoneDataHTTPClient.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol TimeZoneDataHTTPClientDelegate;

@interface TimeZoneDataHTTPClient : AFHTTPSessionManager

@property (nonatomic, weak) id<TimeZoneDataHTTPClientDelegate>delegate;

+ (TimeZoneDataHTTPClient *)sharedTimeZoneDataHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)updateTimeZoneAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString;

@end

@protocol TimeZoneDataHTTPClientDelegate <NSObject>
@optional
-(void)timeZoneDataHTTPClient:(TimeZoneDataHTTPClient *)client didUpdateWithTimeZone:(id)timezone onOriginalLocation:(id)location;
-(void)timeZoneDataHTTPClient:(TimeZoneDataHTTPClient *)client didFailWithError:(NSError *)error;
@end