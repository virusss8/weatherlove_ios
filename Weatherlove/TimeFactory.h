//
//  TimeFactory.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeFactory : NSObject

+ (TimeFactory *)sharedTimeFactoryInstance;

//- (NSString *)getFormattedTimeFromString:(NSString *)date andOffset:(NSNumber *)offset haveToReturnDate:(NSNumber *)isDate haveToReturnTime:(NSNumber *)isTime;
//- (NSString *)getFormattedTimeFromDateWithOffset:(NSNumber *)offset;
//- (NSString *)getApiOriginalFormattedTimeFromDateWithOffset:(NSNumber *)offset;
//- (NSString *)getFormattedDateFromString:(NSString *)date;
//- (NSString *)getWeekdayFromString:(NSString *)date;
- (NSString *)getTransformedMilitaryTimeIn24Format:(NSString *)militaryTime;
//- (NSString *)getUTCTime:(NSString *)currentLocalTime;

- (NSDate *)getUTCDateTimeFromLocalDate:(NSString *)localTime;
- (NSDate *)getUTCDateTimeFromUTCDate:(NSString *)localTime;
- (NSDate *)getUTCDateFromUTCDateDailyWWO:(NSString *)utcDate;
- (NSDate *)getUTCDateTimeFromUTCDateHourlyWWO:(NSString *)localTime;
- (NSDate *)getUTCDateTimeFromUTCDateTimezoneWWO:(NSString *)localTime;
- (int)getIconContextFromDate:(NSDate *)date withSunrise:(NSDate *)sunrise withSunset:(NSDate *)sunset;
- (int)getCurrentIconContexFromDate:(NSDate *)date withOffset:(NSNumber *)offset withSunrise:(NSDate *)sunrise withSunset:(NSDate *)sunset;
- (NSArray *)getFormattedDateTimeFromDate:(NSDate *)date withOffset:(NSNumber *)offset;

//- (NSString *)get24FormatTimeFrom:(NSString *)time;
//- (int)getIconContextFromDate:(NSString *)date withOffset:(NSNumber *)offset withSunrise:(NSString *)sunrise withSunset:(NSString *)sunset;

@end
