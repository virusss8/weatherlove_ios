//
//  ViewController.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 18/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize locationManager,
            weatherForecastDaily,
            weatherForecastHourly,
            timezoneData,
            isWeatherDailySuccessfullyDownloaded,
            isWeatherHourlySuccessfullyDownloaded,
            isWeatherTimezoneSuccessfullyDownloaded;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setIsWeatherDailySuccessfullyDownloaded:NO];
    [self setIsWeatherHourlySuccessfullyDownloaded:NO];
    [self setIsWeatherTimezoneSuccessfullyDownloaded:NO];
	
    self.navigationController.toolbarHidden = NO;
    
    
//    NSString *currentLocalTime = @"2014-03-20 10:28 PM";
//    NSString *gmtString;
//    NSDate *gmtDate;
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * 1.0]];
//    
//    gmtDate = [dateFormatter dateFromString:currentLocalTime];
//    NSLog(@"GMT date: %@", gmtDate);
//    
//    [dateFormatter setDateFormat:@"yyyy..MM..dd HH:mm"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    
//    gmtString = [dateFormatter stringFromDate:gmtDate];
//    
//    NSLog(@"GMT string: %@", gmtString);
    
    
//    NSString *string = @"2014-03-09";
//    NSString *newString;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    NSDate *date = [dateFormatter dateFromString:string];
//    newString = [dateFormatter stringFromDate:date];
//    NSLog(@"default date: %@", newString);
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    date = [dateFormatter dateFromString:string];
//    newString = [dateFormatter stringFromDate:date];
//    NSLog(@"formatted date: %@", newString);
    
    
    
//    double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
//    int seconds = 3600;
//    double obtainedTimezoneOffset = -7.0;
//    NSString *string = @"2014-03-11 09:00";
//    NSString *newString;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    NSDate *date = [dateFormatter dateFromString:string];
//    newString = [dateFormatter stringFromDate:date];
//    NSLog(@"default date: %@", newString);
//    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:seconds * myTimezoneOffset]]; // NO local time
//    newString = [dateFormatter stringFromDate:date];
//    NSLog(@"formatted date: %@", newString);
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:seconds * obtainedTimezoneOffset]]; // YES local time
//    newString = [dateFormatter stringFromDate:date];
//    NSLog(@"formatted date: %@", newString);
    

    

//    NSString *string2 = @"2014-03-11 09:00";
//    NSString *newString2;
//    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
//    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
//    [dateFormatter2 setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    NSDate *date2 = [dateFormatter2 dateFromString:string2];
//    newString2 = [dateFormatter2 stringFromDate:date2];
//    NSLog(@"default date: %@", newString2);
//    [dateFormatter2 setDateFormat:@"dd.MM.yyyy HH:mm"];
//    newString2 = [dateFormatter2 stringFromDate:date2];
//    NSLog(@"formatted date: %@", newString2);
//    date2 = [dateFormatter2 dateFromString:newString2];
//    NSLog(@"new Date: %@", date2);
//    
//    
//    NSDate *currentDate = [NSDate date];
//    NSLog(@"current date: %@", currentDate);
//    
//    if ([currentDate compare:date] == NSOrderedDescending) {
//        if ([currentDate compare:date2] == NSOrderedAscending) {
//            NSLog(@"trenuten datum je med datumi");
//        }
//    }
    
    
//    NSString *time = @"06:58 PM";
//    NSString *newTime;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"hh:mm a"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    
//    NSDate *d = [dateFormatter dateFromString:time];
//    newTime = [dateFormatter stringFromDate:d];
//    [dateFormatter setDateFormat:@"HH:mm"];
//    newTime = [dateFormatter stringFromDate:d];
//    NSLog(@"24cajt: %@", newTime);
    
    
    
//    NSString *date1 = @"2014-03-11 08:51 AM";
//    NSString *date2 = @"07:51 AM";
//    
//    NSArray *dateArr = [date1 componentsSeparatedByString:@" "];
//    
//    NSString *newDate = [NSString stringWithFormat:@"%@ %@", dateArr[0], date2];
//    
//    NSLog(@"newDate: %@", newDate);
    
    
        
//    if ([Util getSharedPrefs:_WEATHER_FORECAST_HOURLY] != nil) {
//        [self setWeatherForecastHourly:[Util getSharedPrefs:_WEATHER_FORECAST_HOURLY]];
//
//        NSString *apiDateString = [[weatherForecastHourly currentCondition] localObservationDateTime_current];//@"2014-04-03 10:58 AM";
//        NSDateFormatter *apiDateFormatter = [[NSDateFormatter alloc] init];
//        [apiDateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
////        [apiDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//        [apiDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//        NSDate *apiDateDate = [apiDateFormatter dateFromString:apiDateString];
//        NSLog(@"API DATE: %@", apiDateDate);
//        [apiDateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [apiDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//        apiDateString = [apiDateFormatter stringFromDate:apiDateDate];
//        NSLog(@"API DATE reformatted: %@", apiDateString);
//        [apiDateFormatter setDateFormat:@"dd.MM.yyyy h:mm a"];
//        [apiDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//        apiDateString = [apiDateFormatter stringFromDate:apiDateDate];
//        NSLog(@"API DATE reformatted: %@", apiDateString);
//        
//        
//        
//        NSString *currentTimeString;
//        NSString *gmtTimeString;
//        NSString *tokioCurrentTimeSting;
//        NSString *sanFranciscoCurrentTimeString;
//        NSString *kabulCurrentTimeString;
//
//        NSDateFormatter *currentTimeFormat = [[NSDateFormatter alloc] init];
//        NSDateFormatter *gmtTimeFormatter = [[NSDateFormatter alloc] init];
//        NSDateFormatter *tokioCurrentTimeFormatter = [[NSDateFormatter alloc] init];
//        NSDateFormatter *sanFranciscoCurrentTimeFormatter = [[NSDateFormatter alloc] init];
//        NSDateFormatter *kabulCurrentTimeFormatter = [[NSDateFormatter alloc] init];
//
//        NSDate *gmtTimeDate;
//
//        gmtTimeDate = [NSDate date];
//        NSLog(@"gmtTimeDate: %@", gmtTimeDate);
//        [gmtTimeFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [gmtTimeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//        gmtTimeString = [gmtTimeFormatter stringFromDate:gmtTimeDate];
//        NSLog(@"gmtTimeString: %@", gmtTimeString);
//        
//        [currentTimeFormat setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [currentTimeFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600]];
//        currentTimeString = [currentTimeFormat stringFromDate:gmtTimeDate];
//        NSLog(@"currentTimeString: %@", currentTimeString);
//        
//        [tokioCurrentTimeFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [tokioCurrentTimeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*9.0]];
//        tokioCurrentTimeSting = [tokioCurrentTimeFormatter stringFromDate:gmtTimeDate];
//        NSLog(@"tokioCurrentTimeString: %@", tokioCurrentTimeSting);
//        
//        [sanFranciscoCurrentTimeFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [sanFranciscoCurrentTimeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*-8.0]];
//        sanFranciscoCurrentTimeString = [sanFranciscoCurrentTimeFormatter stringFromDate:gmtTimeDate];
//        NSLog(@"sanFranciscoCurrentTimeString: %@", sanFranciscoCurrentTimeString);
//        
//        [kabulCurrentTimeFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//        [kabulCurrentTimeFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4.5]];
//        kabulCurrentTimeString = [kabulCurrentTimeFormatter stringFromDate:gmtTimeDate];
//        NSLog(@"kabulCurrentTimeString: %@", kabulCurrentTimeString);
//        [kabulCurrentTimeFormatter setDateFormat:@"dd.MM.yyyy h:mm a"];
//        [kabulCurrentTimeFormatter setDateFormat:@"dd....MM....yyyy h:mm a"];
//        kabulCurrentTimeString = [kabulCurrentTimeFormatter stringFromDate:gmtTimeDate];
//        NSLog(@"kabulCurrentTimeString: %@", kabulCurrentTimeString);
//
//        
//    } else {
    
        self.locationManager = [[CLLocationManager alloc] init];
        [self.locationManager setDelegate:self];
        [self.locationManager startUpdatingLocation];
    
//    [self saveLocationsCompleteDataStruct];
    
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Last object contains the most recent location
    CLLocation *newLocation = [locations lastObject];
    
    // If the location is more than 5 minutes old, ignore it
    if([newLocation.timestamp timeIntervalSinceNow] > 300)
        return;
    
    [self.locationManager stopUpdatingLocation];
    
    TimeZoneDataHTTPClient *clientTimezone = [TimeZoneDataHTTPClient sharedTimeZoneDataHTTPClient];
    [clientTimezone setDelegate:self];
    [clientTimezone updateTimeZoneAtLocation:newLocation :nil];
    
    WeatherForecastDailyHTTPClient *clientDaily = [WeatherForecastDailyHTTPClient sharedWeatherForecastDailyHTTPClient];
    [clientDaily setDelegate:self];
    [clientDaily updateWeatherForecastAtLocation:newLocation :nil];
    
    WeatherForecastHourlyHTTPClient *clientHourly = [WeatherForecastHourlyHTTPClient sharedWeatherForecastHourlyHTTPClient];
    [clientHourly setDelegate:self];
    [clientHourly updateWeatherForecastAtLocation:newLocation :nil];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude] completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil) {
            if (placemarks && placemarks.count > 0) {
//                NSLog(@"Placemarks: %@", placemarks);
                
                CLPlacemark *plmrk = placemarks[0];
                [_labelLoc setText:[NSString stringWithFormat:@"%f, %f\n%@, %@\n%@\n%@, %@\n%@", newLocation.coordinate.latitude, newLocation.coordinate.longitude, plmrk.thoroughfare, plmrk.subThoroughfare, plmrk.locality, plmrk.postalCode, plmrk.subLocality, plmrk.country]];
//                NSLog(@"placemark.ISOcountryCode = %@", plmrk.ISOcountryCode);
//                NSLog(@"placemark.country = %@", plmrk.country);
//                NSLog(@"placemark.postalCode = %@", plmrk.postalCode);
//                NSLog(@"placemark.administrativeArea = %@", plmrk.administrativeArea);
//                NSLog(@"placemark.locality = %@", plmrk.locality);
//                NSLog(@"placemark.subLocality = %@", plmrk.subLocality);
//                NSLog(@"placemark.subThoroughfare = %@", plmrk.subThoroughfare);
            }
        }
    }];
}

#pragma mark - WeatherForecastDailyHTTPClientDelegate

- (void)weatherForecastDailyHTTPClient:(WeatherForecastDailyHTTPClient *)client didUpdateWithForecast:(id)weatherDaily onOriginalLocation:(id)location
{
    [self setWeatherForecastDaily:weatherDaily];
    
    if ([self isWeatherHourlySuccessfullyDownloaded] == YES && [self isWeatherTimezoneSuccessfullyDownloaded] == YES) {
        [self saveLocationsCompleteDataStruct];
    } else {
        [self setIsWeatherDailySuccessfullyDownloaded:YES];
    }
    
//    [Util setWeatherData:_WEATHER_FORECAST_DAILY withValue:weatherDaily];
//    [self setWeatherForecastDaily:weatherDaily];
    
//    NSLog(@"CurrentCondition:_ %@\n\n", [weatherDaily currentCondition]);
//    NSLog(@"\n\n\nRequest:_ %@\n\n", [weatherDaily request]);
//    NSLog(@"\n\n\nUpcomingWeather:_ %@\n\n", [weatherDaily weatherForecast]);
    
    
//    NSLog(@"CurrentCondition - cloudCover: %@", [[weatherDaily currentCondition] cloudCover]);
//    NSLog(@"CurrentCondition - feelsLikeC: %@", [[weatherDaily currentCondition] feelsLikeC]);
//    NSLog(@"CurrentCondition - feelsLikeF: %@", [[weatherDaily currentCondition] feelsLikeF]);
//    NSLog(@"CurrentCondition - humidity: %@", [[weatherDaily currentCondition] humidity]);
//    NSLog(@"CurrentCondition - isDaytime: %@", [[weatherDaily currentCondition] isDaytime]);
//    NSLog(@"CurrentCondition - localObservationDateTime: %@", [[weatherDaily currentCondition] localObservationDateTime]);
//    NSLog(@"CurrentCondition - observationTime: %@", [[weatherDaily currentCondition] observationTime_current]);
//    NSLog(@"CurrentCondition - precipitation: %@", [[weatherDaily currentCondition] precipitation]);
//    NSLog(@"CurrentCondition - pressure: %@", [[weatherDaily currentCondition] pressure]);
//    NSLog(@"CurrentCondition - tempC: %@", [[weatherDaily currentCondition] tempC]);
//    NSLog(@"CurrentCondition - tempF: %@", [[weatherDaily currentCondition] tempF]);
//    NSLog(@"CurrentCondition - visibility: %@", [[weatherDaily currentCondition] visibility]);
//    NSLog(@"CurrentCondition - weatherCode: %@", [[weatherDaily currentCondition] weatherCode]);
//    NSLog(@"CurrentCondition - weatherDescription: %@", [[weatherDaily currentCondition] weatherDescription]);
//    NSLog(@"CurrentCondition - weatherIconURL: %@", [[weatherDaily currentCondition] weatherIconURL]);
//    NSLog(@"CurrentCondition - windDirection16Point: %@", [[weatherDaily currentCondition] windDirection16Point]);
//    NSLog(@"CurrentCondition - windDirectionDegree: %@", [[weatherDaily currentCondition] windDirectionDegree]);
//    NSLog(@"CurrentCondition - windSpeedKmph: %@", [[weatherDaily currentCondition] windSpeedKmph]);
//    NSLog(@"CurrentCondition - windSpeedMiles: %@", [[weatherDaily currentCondition] windSpeedMiles]);
    
//    NSLog(@"Request - query: %@", [[weatherDaily requestData] query]);
//    NSLog(@"Request - type: %@", [[weatherDaily requestData] type]);
    
//    NSLog(@"WeatherForecast - maxTempC: %@", [[weatherDaily weatherForecast][0] maxTempC]);
//    NSLog(@"WeatherForecast - maxTempF: %@", [[weatherDaily weatherForecast][0] maxTempF]);
//    NSLog(@"WeatherForecast - minTempC: %@", [[weatherDaily weatherForecast][0] minTempC]);
//    NSLog(@"WeatherForecast - minTempF: %@", [[weatherDaily weatherForecast][0] minTempF]);
//    
//    NSLog(@"WeatherForecast - astronomy: %@", [[weatherDaily weatherForecast][0] astronomy]);
//    NSLog(@"WeatherForecast - moonrise: %@", [[[weatherDaily weatherForecast][0] astronomy] moonrise]);
//    NSLog(@"WeatherForecast - moonset: %@", [[[weatherDaily weatherForecast][0] astronomy] moonset]);
//    NSLog(@"WeatherForecast - sunrise: %@", [[[weatherDaily weatherForecast][0] astronomy] sunrise]);
//    NSLog(@"WeatherForecast - sunset: %@", [[[weatherDaily weatherForecast][0] astronomy] sunset]);
//    
//    NSLog(@"WeatherForecast - date: %@", [[weatherDaily weatherForecast][0] dateStringOfForecast]);
    
    
    
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][0] hourly][0] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][0] hourly][0] feelsLikeF]);
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][0] daily] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][0] daily] feelsLikeF]);
//    
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][1] hourly][0] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][1] hourly][0] feelsLikeF]);
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][1] daily] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][1] daily] feelsLikeF]);
//    
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][2] hourly][0] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][2] hourly][0] feelsLikeF]);
//    NSLog(@"WeatherForecast - feelsLikeC: %@", [[[weatherDaily weatherForecast][2] daily] feelsLikeC]);
//    NSLog(@"WeatherForecast - feelsLikeF: %@", [[[weatherDaily weatherForecast][2] daily] feelsLikeF]);
}

- (void)weatherForecastDailyHTTPClient:(WeatherForecastDailyHTTPClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                        message:[NSString stringWithFormat:@"%@",error]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - WeatherForecastHourlyHTTPClientDelegate

- (void)weatherForecastHourlyHTTPClient:(WeatherForecastHourlyHTTPClient *)client didUpdateWithForecast:(id)weatherHourly onOriginalLocation:(id)location
{
    [self setWeatherForecastHourly:weatherHourly];
    
    if ([self isWeatherDailySuccessfullyDownloaded] == YES && [self isWeatherTimezoneSuccessfullyDownloaded] == YES) {
        [self saveLocationsCompleteDataStruct];
    } else {
        [self setIsWeatherHourlySuccessfullyDownloaded:YES];
    }
    
//    [Util setWeatherData:_WEATHER_FORECAST_HOURLY withValue:weatherHourly];
//    [self setWeatherForecastHourly:weatherHourly];
    
//    NSLog(@"CurrentCondition:_ %@\n\n", [weatherHourly currentCondition]);
//    NSLog(@"\n\n\nRequest:_ %@\n\n", [weatherHourly requestData]);
//    NSLog(@"\n\n\nUpcomingWeather:_ %@\n\n", [weatherHourly weatherForecast]);
}

- (void)weatherForecastHourlyHTTPClient:(WeatherForecastHourlyHTTPClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                        message:[NSString stringWithFormat:@"%@",error]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

#pragma mark - TimeZoneDataHTTPClientDelegate

- (void)timeZoneDataHTTPClient:(TimeZoneDataHTTPClient *)client didUpdateWithTimeZone:(id)timezone onOriginalLocation:(id)location
{
    [self setTimezoneData:timezone];
    
    if ([self isWeatherDailySuccessfullyDownloaded] == YES && [self isWeatherHourlySuccessfullyDownloaded] == YES) {
        [self saveLocationsCompleteDataStruct];
    } else {
        [self setIsWeatherTimezoneSuccessfullyDownloaded:YES];
    }
    
//    [Util setWeatherData:_WEATHER_TIMEZONE_DATA withValue:timezone];
//    [self setTimezoneData:timezone];
    
//    NSLog(@"Timezone_query: %@", [timezone timezone_query]);
//    NSLog(@"Timezone_type: %@", [timezone timezone_type]);
//    NSLog(@"Timezone_localtime: %@", [timezone timezone_localTime]);
//    NSLog(@"Timezone_UTCoffset: %@", [timezone timezone_utcOffset]);
}

- (void)timeZoneDataHTTPClient:(TimeZoneDataHTTPClient *)client didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                        message:[NSString stringWithFormat:@"%@",error]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

- (IBAction)clickHourly:(id)sender
{
//    NSLog(@"HOURLY_: %@", [Util getWeatherData:_WEATHER_FORECAST_HOURLY]);
}

- (IBAction)clickDaily:(id)sender
{
//    NSLog(@"DAILY_: %@", [Util getWeatherData:_WEATHER_FORECAST_DAILY]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][0] daily] tempC]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][1] daily] tempC]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][2] daily] tempC]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][3] daily] tempC]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][4] daily] tempC]);
//    NSLog(@"... = %@", [[[[Util getWeatherData:_WEATHER_FORECAST_DAILY] weatherForecast][5] daily] tempC]);
}

- (IBAction)clickTimezone:(id)sender
{
//    NSLog(@"TIMEZONE_: %@", [Util getWeatherData:_WEATHER_TIMEZONE_DATA]);
}

- (IBAction)clickQUERYcurrent:(id)sender
{
//    WeatherReportQueryFactory *weatherReport = [WeatherReportQueryFactory sharedWeatherReportQueryFactoryInstance];
//    [weatherReport setWeatherReportDelegate:self];
//    [weatherReport getCurrentWeatherReport];
    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_FORECAST_DAILY];
//    
//    [Util setWeatherData:_WEATHER_FORECAST_DAILY withValue:weatherForecastDaily];
//    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_FORECAST_DAILY];
    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_FORECAST_HOURLY];
//    
//    [Util setWeatherData:_WEATHER_FORECAST_HOURLY withValue:weatherForecastDaily];
//    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_FORECAST_HOURLY];
    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_TIMEZONE_DATA];
//    
//    [Util setWeatherData:_WEATHER_TIMEZONE_DATA withValue:weatherForecastDaily];
//    
//    weatherForecastDaily = [Util getWeatherData:_WEATHER_TIMEZONE_DATA];
    
//    NSLog(@"CurrentCondition - observationTime: %@", [[weatherForecastDaily currentCondition] gmtObservationDateTime_current]);
    
//    for (int i = 0; i < [[weatherForecastDaily weatherForecast] count]; i++) {
//        NSLog(@"moonset: %@", [[weatherForecastDaily weatherForecast][i] dateMoonset]);
//    }
}

- (IBAction)clickQUERYdaily:(id)sender
{
//    WeatherReportQueryFactory *weatherReport = [WeatherReportQueryFactory sharedWeatherReportQueryFactoryInstance];
//    [weatherReport setWeatherReportDelegate:self];
//    [weatherReport getDailyForecast];
}

- (IBAction)clickQUERYhourly:(id)sender
{
//    WeatherReportQueryFactory *weatherReport = [WeatherReportQueryFactory sharedWeatherReportQueryFactoryInstance];
//    [weatherReport setWeatherReportDelegate:self];
//    [weatherReport getHourlyForecast];
}

- (void)currentWeatherReportCallback:(CurrentConditions *)cc
{
//    NSDate *sysTime = [NSDate date];
//    
//    if (cc != nil) {
//      
//        if ([cc weatherIcon] != nil) {
//            NSLog(@"icon: %@", [cc weatherIcon]);
//            [_weatherIcon setImage:[cc weatherIcon]];
//        } else {
//            NSLog(@"icon is null");
//        }
//        
//        if ([cc temperature] != nil) {
//            NSLog(@"temp: %@", [cc temperature]);
//        } else {
//            NSLog(@"temp is null");
//        }
//        
//        if ([cc description] != nil) {
//            NSLog(@"desc: %@", [cc description]);
//        } else {
//            NSLog(@"desc is null");
//        }
//        
//        if ([cc windSpeed] != nil) {
//            NSLog(@"wind: %@", [cc windSpeed]);
//        } else {
//            NSLog(@"wind is null");
//        }
//        
//        if ([cc windIcon] != nil) {
//            NSLog(@"w_ic: %@", [cc windIcon]);
//            [_windIcon setImage:[cc windIcon]];
//        } else {
//            NSLog(@"w_ic is null");
//        }
//        
//        if ([cc sunrise] != nil) {
//            NSLog(@"sunr: %@", [cc sunrise]);
//        } else {
//            NSLog(@"sunr is null");
//        }
//        
//        if ([cc sunset] != nil) {
//            NSLog(@"suns: %@", [cc sunset]);
//        } else {
//            NSLog(@"suns is null");
//        }
//        
//        if ([cc daily] != nil) {
//            for (int i = 0; i < [[cc daily] count]; i++) {
//                DailyForecast *df = [cc daily][i];
//                
//                NSLog(@"-------------------------------------------");
//                
//                if ([df weekday] != nil) {
//                    NSLog(@"df week: %@", [df weekday]);
//                } else {
//                    NSLog(@"df week is null");
//                }
//                
//                if ([df icon] != nil) {
//                    NSLog(@"df icon: %@", [df icon]);
//                    if (i == 0) {
//                        [_weatherIconDay0 setImage:[df icon]];
//                    } else if (i == 1) {
//                        [_weatherIconDay1 setImage:[df icon]];
//                    } else if (i == 2) {
//                        [_weatherIconDay2 setImage:[df icon]];
//                    } else if (i == 3) {
//                        [_weatherIconDay3 setImage:[df icon]];
//                    } else if (i == 4) {
//                        [_weatherIconDay4 setImage:[df icon]];
//                    }
//                } else {
//                    NSLog(@"df icon is null");
//                }
//                
//                if ([df minTemperature] != nil) {
//                    NSLog(@"df minT: %@", [df minTemperature]);
//                } else {
//                    NSLog(@"df minT is null");
//                }
//                
//                if ([df maxTemperature] != nil) {
//                    NSLog(@"df maxT: %@", [df maxTemperature]);
//                } else {
//                    NSLog(@"df maxT is null");
//                }
//            }
//        }
//    }
//    
//    NSLog(@"sistemska zahtevnost CURRENT: %i millis", (int)(-[sysTime timeIntervalSinceNow] * 1000));
}

- (void)dailyForecastReportCallback:(NSArray *)df
{
//    NSDate *sysTime = [NSDate date];
//    
//    if (df != nil) {
//
//        for (int i = 0; i < [df count]; i++) {
//            DailyForecast *dftmp = df[i];
//            
//            NSLog(@"-------------------------------------------");
//            
//            if ([dftmp weekday] != nil) {
//                NSLog(@"df week: %@", [dftmp weekday]);
//            } else {
//                NSLog(@"df week is null");
//            }
//            
//            if ([dftmp date] != nil) {
//                NSLog(@"df date: %@", [dftmp date]);
//            } else {
//                NSLog(@"df date is null");
//            }
//            
//            if ([dftmp icon] != nil) {
//                NSLog(@"df icon: %@", [dftmp icon]);
//            } else {
//                NSLog(@"df icon is null");
//            }
//            
//            if ([dftmp minTemperature] != nil) {
//                NSLog(@"df minT: %@", [dftmp minTemperature]);
//            } else {
//                NSLog(@"df minT is null");
//            }
//            
//            if ([dftmp maxTemperature] != nil) {
//                NSLog(@"df maxT: %@", [dftmp maxTemperature]);
//            } else {
//                NSLog(@"df maxT is null");
//            }
//            
//            if ([dftmp windSpeed] != nil) {
//                NSLog(@"df wind: %@", [dftmp windSpeed]);
//            } else {
//                NSLog(@"df wind is null");
//            }
//        }
//    }
//    
//    NSLog(@"sistemska zahtevnost DAILY: %i millis", (int)(-[sysTime timeIntervalSinceNow] * 1000));
}

- (void)hourlyForecastReportCallback:(NSArray *)hf
{
//    NSDate *sysTime = [NSDate date];
//    
//    NSLog(@"hourlyForecastReportCallback");
//    
//    NSLog(@"HF: %@", hf);
//    
//    NSLog(@"sistemska zahtevnost HOURLY: %i millis", (int)(-[sysTime timeIntervalSinceNow] * 1000));
//    
//    for (int i = 0; i < [hf count]; i++) {
//        HourlyForecast *hftmp = hf[i];
//        
//        [_weatherIcon setImage:[hftmp icon]];
//    }
}

- (void)saveLocationsCompleteDataStruct
{
//    [Util setSharedPrefs:@"_TZ" withValue:[self timezoneData]];
//    [Util setSharedPrefs:@"_DF" withValue:[self weatherForecastDaily]];
//    [Util setSharedPrefs:@"_HF" withValue:[self weatherForecastHourly]];
//    
    [Util prepareWeatherDataAtIndex:0 :[self timezoneData] :[self weatherForecastDaily] :[self weatherForecastHourly]];
    
//    [Util prepareWeatherData:[Util getSharedPrefs:@"_TZ"] :[Util getSharedPrefs:@"_DF"] :[Util getSharedPrefs:@"_HF"]];
}

@end
