//
//  MetricFactory.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MetricFactory : NSObject

+ (MetricFactory *)sharedMetricFactoryInstance;
- (NSNumber *)getFormattedMetricsInKmph:(NSNumber *)kmph orMph:(NSNumber *)mph;

@end
