//
//  WeatherReportQueryFactory.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 05/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "WeatherReportQueryFactory.h"

@implementation WeatherReportQueryFactory

+ (WeatherReportQueryFactory *)sharedWeatherReportQueryFactoryInstance
{
    static WeatherReportQueryFactory *_sharedWeatherReportQueryFactoryInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherReportQueryFactoryInstance = [[self alloc] init];
    });
    
    return _sharedWeatherReportQueryFactoryInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self setStructs];
    }
    return self;
}

- (void)setStructs
{
    if (_savedDailyForecast == nil) {
        _savedDailyForecast = [Util getWeatherData:_WEATHER_FORECAST_DAILY];
    }
    
    if (_savedHourlyForecast == nil) {
        _savedHourlyForecast = [Util getWeatherData:_WEATHER_FORECAST_HOURLY];
    }
    
    if (_savedTimezone == nil) {
        _savedTimezone = [Util getWeatherData:_WEATHER_TIMEZONE_DATA];
    }
    if (_day == nil) {
        _day = [self getRightForecastDay];
    }
    
    if (_daysAvailable == nil) {
        _daysAvailable = [self getAvailableForecastDay];
    }
}

#pragma mark - main methods

- (NSNumber *)getRightForecastDay
{
    NSDate *sysTime = [NSDate date];
    
    NSArray *weatherTemp = [_savedDailyForecast weatherForecast];
    NSDate *currentDate = [NSDate date];
    NSDate *fcDate;
    NSString *currentString;
    NSString *fcString;
    NSDateFormatter *compFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *fcDateFormatter = [[NSDateFormatter alloc] init];
    [fcDateFormatter setDateFormat:@"yyyy-MM-dd"];
    [fcDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [compFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    BOOL isRightTime = YES;
    
    for (int day = 0; day < [weatherTemp count]; day++) {
        fcDate = [fcDateFormatter dateFromString:[weatherTemp[day] dateStringOfForecast]];
        fcString = [fcDateFormatter stringFromDate:fcDate];
//        NSDate *tmp = [fcDateFormatter dateFromString:fcString];
//        NSLog(@"datum: %@", tmp);
        
        [compFormatter setDateFormat:@"yyyy"];
        fcString = [compFormatter stringFromDate:fcDate];
        currentString = [compFormatter stringFromDate:currentDate];
        
        if ([fcString isEqualToString:currentString]) {
            isRightTime = YES;
        } else {
            isRightTime = NO;
        }
        
//        NSLog(@"datum yyyy: %@ - day - %i", fcString, day);
//        NSLog(@"datum yyyy: %@ - day - %i", currentString, day);
        
        [compFormatter setDateFormat:@"MM"];
        fcString = [compFormatter stringFromDate:fcDate];
        currentString = [compFormatter stringFromDate:currentDate];
        
        if ([fcString isEqualToString:currentString]) {
            isRightTime = YES;
        } else {
            isRightTime = NO;
        }
        
//        NSLog(@"datum MM: %@ - day - %i", fcString, day);
//        NSLog(@"datum MM: %@ - day - %i", currentString, day);
        
        [compFormatter setDateFormat:@"dd"];
        fcString = [compFormatter stringFromDate:fcDate];
        currentString = [compFormatter stringFromDate:currentDate];
        
//        NSLog(@"datum dd: %@ - day - %i", fcString, day);
//        NSLog(@"datum dd: %@ - day - %i", currentString, day);
        
        if ([fcString isEqualToString:currentString] && isRightTime == YES) {
            return [NSNumber numberWithInt:day];
            NSLog(@"datum    USTREZA - %i", day);
        }
    }
    NSLog(@"datum NE USTREZA");
    NSLog(@"sistemska zahtevnost: %i millis", (int)(-[sysTime timeIntervalSinceNow] * 1000));
    return nil;
}

- (NSNumber *)getAvailableForecastDay
{
    return [NSNumber numberWithInt:[[_savedDailyForecast weatherForecast] count] - [_day intValue]];
}

- (/*CurrentConditions **/void)getCurrentWeatherReport
{
    [self setStructs];
    
    CurrentConditions *currentConditions = [CurrentConditions sharedCurrentConditionsInstance];
    
    if (_day == nil) { // there is no more forecast available - user must update forecast if he want to see data
        
        currentConditions = [self setCurrentConditionsNullStruct];
        
    } else if ([_day intValue] == 0) { // forecast is up to date and i can use current conditions
        
        currentConditions = [self setCurrentConditionsUpToDateStruct];
        
    } else { // the forecast is a bit old, there might not be all 15 days of forecast available - user better update
        
        currentConditions = [self setCurrentConditionsNotUpToDateStruct];
    }
    
    if ([self.weatherReportDelegate respondsToSelector:@selector(currentWeatherReportCallback:)]) {
        [self.weatherReportDelegate currentWeatherReportCallback:currentConditions];
    } else {
        NSLog(@"currentWeatherReportCallback is not delegated!");
    }
}

- (/*NSArray **/void)getHourlyForecast
{
    [self setStructs];
    
    NSArray *hourlyForecast;
    
    if (_day == nil) { // there is no more forecast available - user must update forecast if he want to see data
        
        hourlyForecast = [self setHourlyForecastNullStruct];
        
    } else {
        
        hourlyForecast = [self setHourlyForecastStruct];
    }
    
    if ([self.weatherReportDelegate respondsToSelector:@selector(hourlyForecastReportCallback:)]) {
        [self.weatherReportDelegate hourlyForecastReportCallback:hourlyForecast];
    } else {
        NSLog(@"hourlyForecastReportCallback is not delegated!");
    }
}

- (/*NSArray **/void)getDailyForecast
{
    [self setStructs];
    
    NSArray *dailyForecast;
    
    if (_day == nil) { // there is no more forecast available - user must update forecast if he want to see data
        
        dailyForecast = [self setDailyForecastNullStruct];
        
    } else {
        
        dailyForecast = [self setDailyForecastStruct];
    }
    
    if ([self.weatherReportDelegate respondsToSelector:@selector(dailyForecastReportCallback:)]) {
        [self.weatherReportDelegate dailyForecastReportCallback:dailyForecast];
    } else {
        NSLog(@"dailyForecastReportCallback is not delegated!");
    }
}

# pragma mark - internal CurrentConditions methods

- (CurrentConditions *)setCurrentConditionsNullStruct
{
//    CurrentConditions *currentConditions = [CurrentConditions sharedCurrentConditionsInstance];
//    
//    [currentConditions setWeatherIcon:nil];
//    [currentConditions setTemperature:nil];
//    [currentConditions setDescription:nil];
//    [currentConditions setWindSpeed:nil];
//    [currentConditions setWindIcon:nil];
//    [currentConditions setSunrise:nil];
//    [currentConditions setSunset:nil];
//    [currentConditions setDaily:nil];
//    
//    return currentConditions;
    
    return nil;
}

- (CurrentConditions *)setCurrentConditionsUpToDateStruct
{
    CurrentConditions *currentConditions = [CurrentConditions sharedCurrentConditionsInstance];
//    IconFactory *iconFactory = [IconFactory sharedIconFactoryInstance];
//    DegreeFactory *degreeFactory = [DegreeFactory sharedDegreeFactoryInstance];
//    MetricFactory *metricFactory = [MetricFactory sharedMetricFactoryInstance];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
//    
//    // weather icon
//    [currentConditions setWeatherIcon:[iconFactory getWeatherIconImageFromCode:[[_savedDailyForecast currentCondition] weatherCode_current] andWithDayNightContext:[timeFactory getCurrentIconContexFromDate:[NSDate date] withOffset:[_savedTimezone timezone_utcOffset] withSunrise:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunrise] withSunset:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunset]]]];
//    
//    // temp_C or temp_F
//    [currentConditions setTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast currentCondition] tempC_current] orFahrenheit:[[_savedDailyForecast currentCondition] tempF_current]]];
//    
//    // weather description
//    [currentConditions setDescription:[[_savedDailyForecast currentCondition] weatherDescription_current]];
//    
//    // windSpeedKmph or windSpeedMph
//    [currentConditions setWindSpeed:[metricFactory getFormattedMetricsInKmph:[[_savedDailyForecast currentCondition] windSpeedKmph_current] orMph:[[_savedDailyForecast currentCondition] windSpeedMiles_current]]];
//    
//    // wind icon
//    [currentConditions setWindIcon:[iconFactory getWindIconImage:[[_savedDailyForecast currentCondition] windDirection16Point_current]]];
//    
//    // sunrise time (depends of 12-24 timeformat) (depends of localtime or theirs) (depends of zero timeformat)
//    NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunrise] withOffset:[_savedTimezone timezone_utcOffset]];
//    if ([tmp count] == 2) {
//        [currentConditions setSunrise:tmp[0]];
//    }
//    
//    // sunset time (depends of 12-24 timeformat) (depends of localtime or theirs) (depends of zero timeformat)
//    tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunset] withOffset:[_savedTimezone timezone_utcOffset]];
//    if ([tmp count] == 2) {
//        [currentConditions setSunset:tmp[0]];
//    }
//    
//    // array mini - 4 days ahead
//    NSMutableArray *array = [[NSMutableArray alloc] init];
//    DailyForecast *dailyForecast = [DailyForecast sharedDailyForecastInstance];
//    
//    for (int day = [_day intValue]; day < [_CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST intValue]; day++) {
//        
//        NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][day] dateOfForecast] withOffset:[_savedTimezone timezone_utcOffset]];
//        
//        // day of the week
////        [dailyForecast setWeekday:[timeFactory getWeekdayFromString:[[_savedDailyForecast weatherForecast][day] dateStringOfForecast]]];
//        if (tmp != nil && [tmp count] == 3) {
//            [dailyForecast setWeekday:tmp[2]];
//        }
//        //        NSLog(@"df_weekday = %@", dailyForecast.weekday);
//        
//        // weather icon
//        [dailyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedDailyForecast weatherForecast][day] daily] weatherCode] andWithDayNightContext:ICON_CONTEXT_DAILY]];
//        //        NSLog(@"df_icon = %@", dailyForecast.icon);
//        
//        // minTempC or minTempF
//        [dailyForecast setMinTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] minTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] minTempF]]];
//        //        NSLog(@"df_mintemp = %@", dailyForecast.minTemperature);
//        
//        // maxTempC or maxTempF
//        [dailyForecast setMaxTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] maxTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] maxTempF]]];
//        //        NSLog(@"df_maxtemp = %@", dailyForecast.maxTemperature);
//        
//        //        NSLog(@"dailyForecast = %@", dailyForecast);
//        
//        [array addObject:dailyForecast];
//        
//        dailyForecast = [DailyForecast new];
//    }
//    
//    [currentConditions setDaily:array];
    
    return currentConditions;
}

- (CurrentConditions *)setCurrentConditionsNotUpToDateStruct
{
    CurrentConditions *currentConditions = [CurrentConditions sharedCurrentConditionsInstance];
//    IconFactory *iconFactory = [IconFactory sharedIconFactoryInstance];
//    DegreeFactory *degreeFactory = [DegreeFactory sharedDegreeFactoryInstance];
//    MetricFactory *metricFactory = [MetricFactory sharedMetricFactoryInstance];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
//    
//    // weather icon
//    [currentConditions setWeatherIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] weatherCode] andWithDayNightContext:[timeFactory getCurrentIconContexFromDate:[NSDate date] withOffset:[_savedTimezone timezone_utcOffset] withSunrise:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunrise] withSunset:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunset]]]];
//    
//    // temp_C or temp_F
//    [currentConditions setTemperature:[degreeFactory getFormattedDegreeInCelsius:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] tempC] orFahrenheit:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] tempF]]];
//    
//    // weather description
//    [currentConditions setDescription:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] weatherDescription]];
//    
//    // windSpeedKmph or windSpeedMph
//    [currentConditions setWindSpeed:[metricFactory getFormattedMetricsInKmph:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] windSpeedKmph] orMph:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] windSpeedMiles]]];
//    
//    // wind icon
//    [currentConditions setWindIcon:[iconFactory getWindIconImage:[[[_savedDailyForecast weatherForecast][[_day intValue]] daily] windDirection16Point]]];
//    
//    // sunrise time (depends of 12-24 timeformat) (depends of localtime or theirs) (depends of zero timeformat)
//    NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunrise] withOffset:[_savedTimezone timezone_utcOffset]];
//    if (tmp != nil && [tmp count] == 3) {
//        [currentConditions setSunrise:tmp[0]];
//    }
////    NSLog(@"date outside: %@", [[_savedDailyForecast weatherForecast][[_day intValue]] dateSunrise]);
////    NSLog(@"offset: %@", [_savedTimezone timezone_utcOffset]);
//    
//    // sunset time (depends of 12-24 timeformat) (depends of localtime or theirs) (depends of zero timeformat)
//    tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][[_day intValue]] dateSunset] withOffset:[_savedTimezone timezone_utcOffset]];
//    if (tmp != nil && [tmp count] == 3) {
//        [currentConditions setSunset:tmp[0]];
//    }
//    
//    // array mini - 4 days ahead
//    NSMutableArray *array = [[NSMutableArray alloc] init];
//    DailyForecast *dailyForecast = [DailyForecast sharedDailyForecastInstance];
//    
//    int endIndex;
//    if ([_daysAvailable intValue] >= [_CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST intValue]) {
//        endIndex = [_day intValue] + [_CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST intValue];
//    } else if ([_daysAvailable intValue] < [_CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST intValue]){
//        endIndex = [_daysAvailable intValue];
//    } else {
//        endIndex = [_CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST intValue];
//    }
//    
//    for (int day = [_day intValue]; day < endIndex; day++) {
//        
//        NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][day] dateOfForecast] withOffset:[_savedTimezone timezone_utcOffset]];
//        
//        // day of the week
////        [dailyForecast setWeekday:[timeFactory getWeekdayFromString:[[_savedDailyForecast weatherForecast][day] dateStringOfForecast]]];
//        if (tmp != nil && [tmp count] == 3) {
//            [dailyForecast setWeekday:tmp[2]];
//        }
//        //        NSLog(@"df_weekday = %@", dailyForecast.weekday);
//        
//        // weather icon
//        [dailyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedDailyForecast weatherForecast][day] daily] weatherCode] andWithDayNightContext:ICON_CONTEXT_DAILY]];
//        //        NSLog(@"df_icon = %@", dailyForecast.icon);
//        
//        // minTempC or minTempF
//        [dailyForecast setMinTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] minTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] minTempF]]];
//        //        NSLog(@"df_mintemp = %@", dailyForecast.minTemperature);
//        
//        // maxTempC or maxTempF
//        [dailyForecast setMaxTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] maxTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] maxTempF]]];
//        //        NSLog(@"df_maxtemp = %@", dailyForecast.maxTemperature);
//        
//        //        NSLog(@"dailyForecast = %@", dailyForecast);
//        
//        [array addObject:dailyForecast];
//        
//        dailyForecast = [DailyForecast new];
//    }
//    
//    [currentConditions setDaily:array];
    
    return currentConditions;
}

# pragma mark - internal DailyForecast methods

- (NSArray *)setDailyForecastNullStruct
{
    return nil;
}

- (NSArray *)setDailyForecastStruct
{
    IconFactory *iconFactory = [IconFactory sharedIconFactoryInstance];
    DegreeFactory *degreeFactory = [DegreeFactory sharedDegreeFactoryInstance];
    MetricFactory *metricFactory = [MetricFactory sharedMetricFactoryInstance];
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
    // array max - 15 days ahead
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
//    DailyForecast *dailyForecast = [DailyForecast sharedDailyForecastInstance];
//    
//    for (int day = [_day intValue]; day < [[_savedDailyForecast weatherForecast] count]; day++) {
//        
//        NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[_savedDailyForecast weatherForecast][day] dateOfForecast] withOffset:[_savedTimezone timezone_utcOffset]];
//        
//        // day of the week
////        [dailyForecast setWeekday:[timeFactory getWeekdayFromString:[[_savedDailyForecast weatherForecast][day] dateStringOfForecast]]];
//        if (tmp != nil && [tmp count] == 3) {
//            [dailyForecast setWeekday:tmp[2]];
//        }
////        NSLog(@"weekdsay: %@", [dailyForecast weekday]);
//        
//        // date in selected format (sre, mar. 5 etc.)
////        [dailyForecast setDate:[timeFactory getFormattedDateFromString:[[_savedDailyForecast weatherForecast][day] dateStringOfForecast]]];
//        if (tmp != nil && [tmp count] == 3) {
//            [dailyForecast setDate:tmp[1]];
//        }
////        NSLog(@"date: %@", [dailyForecast date]);
//        
//        // weather icon
//        [dailyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedDailyForecast weatherForecast][day] daily] weatherCode] andWithDayNightContext:ICON_CONTEXT_DAILY]];
////        NSLog(@"icon: %@", [dailyForecast icon]);
//        
//        // minTempC or minTempF
//        [dailyForecast setMinTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] minTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] minTempF]]];
////        NSLog(@"minTemp: %@", [dailyForecast minTemperature]);
//        
//        // maxTempC or maxTempF
//        [dailyForecast setMaxTemperature:[degreeFactory getFormattedDegreeInCelsius:[[_savedDailyForecast weatherForecast][day] maxTempC] orFahrenheit:[[_savedDailyForecast weatherForecast][day] maxTempF]]];
////        NSLog(@"maxTemp: %@", [dailyForecast maxTemperature]);
//        
//        // windSpeedKMH or windSpeedMILES
//        [dailyForecast setWindSpeed:[metricFactory getFormattedMetricsInKmph:[[[_savedDailyForecast weatherForecast][day] daily] windSpeedKmph] orMph:[[[_savedDailyForecast weatherForecast][day] daily] windSpeedMiles]]];
////        NSLog(@"windSpeed: %@", [dailyForecast windSpeed]);
//        
//        [array addObject:dailyForecast];
//        
//        dailyForecast = [DailyForecast new];
//    }
    
    return array;
}

# pragma mark - internal HourlyForecast methods

- (NSArray *)setHourlyForecastNullStruct
{
    return nil;
}

- (NSArray *)setHourlyForecastStruct
{
    IconFactory *iconFactory = [IconFactory sharedIconFactoryInstance];
    DegreeFactory *degreeFactory = [DegreeFactory sharedDegreeFactoryInstance];
    MetricFactory *metricFactory = [MetricFactory sharedMetricFactoryInstance];
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
//    HourlyForecast *hourlyForecast = [HourlyForecast sharedHourlyForecastInstance];
//    
//    // array of hours 15 days * 8 hourly per day
//    for (int day = [_day intValue]; day < [[_savedHourlyForecast weatherForecast] count]; day++) {
//        
////        NSLog(@"------------------------");
//        
//        for (int hour = 0; hour < [[[_savedHourlyForecast weatherForecast][day] hourly] count]; hour++) {
//            
////            NSLog(@"DAY - %i --- HOUR - %i", day, hour);
//            
//            NSLog(@"DAJT: %@", [[[_savedHourlyForecast weatherForecast][day] hourly][hour] UTCDateTime]);
//            
//            NSArray *tmp = [timeFactory getFormattedDateTimeFromDate:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] UTCDateTime] withOffset:[_savedTimezone timezone_utcOffset]];
//            // hour (depends of 12-24 timeformat) (depends of localtime or theirs) (depends of zero timeformat)
////            [hourlyForecast setHour:[timeFactory getFormattedTimeFromString:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] UTCDateTime] andOffset:[_savedTimezone timezone_utcOffset] haveToReturnDate:[NSNumber numberWithBool:NO] haveToReturnTime:[NSNumber numberWithBool:YES]]];
//            if (tmp != nil && [tmp count] == 3) {
//                [hourlyForecast setHour:tmp[0]];
//            }
//            NSLog(@"hour: %@", [hourlyForecast hour]);
//            
//            // date in selected format (sre, mar. 5 etc.)
////            [hourlyForecast setDate:[timeFactory getFormattedTimeFromString:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] UTCDateTime] andOffset:[_savedTimezone timezone_utcOffset] haveToReturnDate:[NSNumber numberWithBool:YES] haveToReturnTime:[NSNumber numberWithBool:NO]]];
//            if (tmp != nil && [tmp count] == 3) {
//                [hourlyForecast setDate:tmp[1]];
//            }
//            NSLog(@"date: %@", [hourlyForecast date]);
//            
//            // weather icon
////            [hourlyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] weatherCode] andWithDayNightContext:[timeFactory getIconContextFromDate:[timeFactory getApiOriginalFormattedTimeFromDateWithOffset:[_savedTimezone timezone_utcOffset]] withOffset:[_savedTimezone timezone_utcOffset] withSunrise:[[[_savedHourlyForecast weatherForecast][day] daily] sunrise] withSunset:[[[_savedHourlyForecast weatherForecast][day] daily] sunset]]]];
//            if ([[[[_savedTimezone weatherForecast][day] hourly][hour] isDayTime] boolValue] == YES) {
//                [hourlyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] weatherCode] andWithDayNightContext:ICON_CONTEXT_DAILY]];
//            } else {
//                [hourlyForecast setIcon:[iconFactory getWeatherIconImageFromCode:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] weatherCode] andWithDayNightContext:ICON_CONTEXT_NIGHTLY]];
//            }
//            NSLog(@"_______________________________");
////            NSLog(@"icon: %@", [hourlyForecast icon]);
//            NSLog(@"sunrise___: %@", [[_savedHourlyForecast weatherForecast][day] dateSunrise]);
//            NSLog(@"sunset____: %@", [[_savedHourlyForecast weatherForecast][day] dateSunset]);
//            NSLog(@"_______________________________");
//            
//            // tempC of tempF
//            [hourlyForecast setTemperature:[degreeFactory getFormattedDegreeInCelsius:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] tempC] orFahrenheit:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] tempF]]];
//            NSLog(@"temp: %@", [hourlyForecast temperature]);
//            
//            // windSpeedKMH or windSpeedMILES
//            [hourlyForecast setWindSpeed:[metricFactory getFormattedMetricsInKmph:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] windSpeedKmph] orMph:[[[_savedHourlyForecast weatherForecast][day] hourly][hour] windSpeedMiles]]];
//            NSLog(@"wind: %@", [hourlyForecast windSpeed]);
//            
//            [array addObject:hourlyForecast];
//            
//            hourlyForecast = [HourlyForecast new];
//        }
//    }
    
    return array;
}

@end
