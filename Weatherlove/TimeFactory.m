//
//  TimeFactory.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "TimeFactory.h"

@implementation TimeFactory

+ (TimeFactory *)sharedTimeFactoryInstance
{
    static TimeFactory *_sharedTimeFactoryInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedTimeFactoryInstance = [[self alloc] init];
    });
    
    return _sharedTimeFactoryInstance;
}

- (NSString *)getFormattedTimeFromString:(NSString *)date andOffset:(NSNumber *)offset haveToReturnDate:(NSNumber *)isDate haveToReturnTime:(NSNumber *)isTime
{
    if (isDate != nil && isTime != nil) {
        double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
//        NSLog(@"location_offset: %f", [offset doubleValue]);
//        NSLog(@"date: %@", date);
//        NSLog(@"my_offset: %f", myTimezoneOffset);

        //    double obtainedTimezoneOffset = -7.0;
        //    NSString *string = @"2014-03-11 09:00";
        NSString *newString;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//        [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        NSDate *d = [dateFormatter dateFromString:date];
        newString = [dateFormatter stringFromDate:d];
//        NSLog(@"default date: %@", newString);
        
        if ([[Util getSharedPrefs:_USES_LOCAL_TIME_VAR_NAME] boolValue] == YES) {
            //        [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
//            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * myTimezoneOffset]]; // YES local time
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * [offset doubleValue]]];
            newString = [dateFormatter stringFromDate:d];
            //            NSLog(@"formatted date: %@", newString);
        } else if ([[Util getSharedPrefs:_USES_LOCAL_TIME_VAR_NAME] boolValue] == NO) {
//            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * (myTimezoneOffset - [offset doubleValue])]]; // NO local time
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * myTimezoneOffset]];
            newString = [dateFormatter stringFromDate:d];
            //            NSLog(@"formatted date: %@", newString);
        }
        
        NSString *timeForm;
        if ([[Util getSharedPrefs:_HOUR_FORMAT_VAR_NAME] intValue] == HOUR_FORMAT_24) {
            if ([[Util getSharedPrefs:_USES_LONG_HOUR_FORMAT_VAR_NAME] boolValue] == YES) {
                timeForm = @"HH:mm";
            } else {
                timeForm = @"H:mm";
            }
        } else if ([[Util getSharedPrefs:_HOUR_FORMAT_VAR_NAME] intValue] == HOUR_FORMAT_AM_PM) {
            if ([[Util getSharedPrefs:_USES_LONG_HOUR_FORMAT_VAR_NAME] boolValue] == YES) {
                timeForm = @"hh:mm a";
            } else {
                timeForm = @"h:mm a";
            }
        }
        
        NSString *dateForm;
        dateForm = @"dd.MM.YYYY";
//        dateForm = @"MM/dd/YYYY";
//        dateForm = @"dd/MM/YYYY";
//        dateForm = @"YYYY/MM/dd";
//        dateForm = @"dd MMM.YYYY";
//        dateForm = @"MMM. dd,YYYY";
//        dateForm = @"EEE. dd MMM.";
//        dateForm = @"EEE., MMM. dd";
//        dateForm = @"MMMM dd";
//        dateForm = @"dd MMMM";
//        dateForm = @"EEEE dd MMMM";
//        dateForm = @"EEEE, MMMM dd";
//        dateForm = @"dd.MM.YY";
//        dateForm = @"EEE., dd.MMM.";
        
        if ([isDate boolValue] == YES && [isTime boolValue] == YES) {
            [dateFormatter setDateFormat:[NSString stringWithFormat:@"%@ %@", dateForm, timeForm]];
        } else if ([isDate boolValue] == YES && [isTime boolValue] == NO) {
            [dateFormatter setDateFormat:[NSString stringWithFormat:@"%@", dateForm]];
        } else if ([isDate boolValue] == NO && [isTime boolValue] == YES) {
            [dateFormatter setDateFormat:[NSString stringWithFormat:@"%@", timeForm]];
        } else {
            return nil;
        }
        
        newString = [dateFormatter stringFromDate:d];
//        NSLog(@"newString: %@", newString);
        
        return newString;
        
    } else {
        return nil;
    }
}

- (NSString *)getFormattedTimeFromDateWithOffset:(NSNumber *)offset
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
    NSLog(@"gmtTimeDate: %@", date);
    [dateFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * myTimezoneOffset]];
    NSString *newString = [dateFormatter stringFromDate:date];
    NSLog(@"gmtTimeString: %@", newString);
    
    return newString;
}

- (NSString *)getApiOriginalFormattedTimeFromDateWithOffset:(NSNumber *)offset
{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
    NSLog(@"gmtTimeDate: %@", date);
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * myTimezoneOffset]];
    NSString *newString = [dateFormatter stringFromDate:date];
    NSLog(@"gmtTimeString: %@", newString);
    
    return newString;
}

- (NSString *)getFormattedDateFromString:(NSString *)date
{
// REWORK for settings
    
    NSString *str;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSDate *d = [dateFormatter dateFromString:date];
    str = [dateFormatter stringFromDate:d];
    
    NSString *dateFormat;
    dateFormat = @"dd.MM.YYYY";
    //        dateFormat = @"MM/dd/YYYY";
    //        dateFormat = @"dd/MM/YYYY";
    //        dateFormat = @"YYYY/MM/dd";
    //        dateFormat = @"dd MMM.YYYY";
    //        dateFormat = @"MMM. dd,YYYY";
    //        dateFormat = @"EEE. dd MMM.";
    //        dateFormat = @"EEE., MMM. dd";
    //        dateFormat = @"MMMM dd";
    //        dateFormat = @"dd MMMM";
    //        dateFormat = @"EEEE dd MMMM";
    //        dateFormat = @"EEEE, MMMM dd";
    //        dateFormat = @"dd.MM.YY";
    //        dateFormat = @"EEE., dd.MMM.";
    
    [dateFormatter setDateFormat:dateFormat];
    str = [dateFormatter stringFromDate:d];
    
    return str;
}

//- (NSNumber *)getIndexOfDayForecast:(NSArray *)forecast
//{
//    NSDate *currentDateTIme = [NSDate date];
//    
//}

//- (NSString *)getWeekdayFromString:(NSString *)date
//{
////    NSLog(@"getWeekday_date = %@", date);
//    NSString *str;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    NSDate *d = [dateFormatter dateFromString:date];
//    str = [dateFormatter stringFromDate:d];
//    [dateFormatter setDateFormat:@"EEEE"];
//    str = [dateFormatter stringFromDate:d];
////    NSLog(@"str: %@", str);
//    
//    return str;
//}

- (NSString *)getTransformedMilitaryTimeIn24Format:(NSString *)militaryTime
{
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i = 0; i < [militaryTime length]; i++) {
        [array addObject:[NSString stringWithFormat:@"%C", [militaryTime characterAtIndex:i]]];
    }
    
    if ([array count] == 1) {
        return [NSString stringWithFormat:@"%@%@:%@%@", [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0]];
    } else if ([array count] == 3) {
        return [NSString stringWithFormat:@"%@%@:%@%@", [NSNumber numberWithInt:0], array[0], array[1], array[2]];
    } else if ([array count] == 4) {
        return [NSString stringWithFormat:@"%@%@:%@%@", array[0], array[1], array[2], array[3]];
    } else {
        return [NSString new];
    }
}

//- (NSString *)getUTCTime:(NSString *)currentLocalTime
//{    
//    NSString *gmtString;
//    NSDate *gmtDate;
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * [[[Util getWeatherData:_WEATHER_TIMEZONE_DATA] timezone_utcOffset] doubleValue]]];
//    
//    gmtDate = [dateFormatter dateFromString:currentLocalTime];
//    NSLog(@"GMT date: %@", gmtDate);
//    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    
//    gmtString = [dateFormatter stringFromDate:gmtDate];
//    
////    NSLog(@"GMT string: %@", gmtString);
//    
//    return gmtString;
//}

- (NSDate *)getUTCDateTimeFromLocalDate:(NSString *)localTime
{
    NSDate *gmtDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    double offset = [[[Util getWeatherData:_WEATHER_TIMEZONE_DATA] timezone_utcOffset] doubleValue];
//    NSLog(@"offset: %f", offset);
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * offset]];
    
    gmtDate = [dateFormatter dateFromString:localTime];
//    NSLog(@"GMT date: %@", gmtDate);
    
    return gmtDate;
}

- (NSDate *)getUTCDateTimeFromUTCDate:(NSString *)localTime
{
    NSDate *gmtDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * 0]];
    
    gmtDate = [dateFormatter dateFromString:localTime];
    //    NSLog(@"GMT date: %@", gmtDate);
    
    return gmtDate;
}

- (NSDate *)getUTCDateFromUTCDateDailyWWO:(NSString *)utcDate
{
    NSDate *gmtDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * 0]];
    
    gmtDate = [dateFormatter dateFromString:utcDate];
    //    NSLog(@"GMT date: %@", gmtDate);
    
    return gmtDate;
}

- (NSDate *)getUTCDateTimeFromUTCDateHourlyWWO:(NSString *)localTime
{
    NSDate *gmtDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * 0]];
    
    gmtDate = [dateFormatter dateFromString:localTime];
    //    NSLog(@"GMT date: %@", gmtDate);
    
    return gmtDate;
}

- (NSDate *)getUTCDateTimeFromUTCDateTimezoneWWO:(NSString *)localTime
{
    NSDate *gmtDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    double offset = [[[Util getWeatherData:_WEATHER_TIMEZONE_DATA] timezone_utcOffset] doubleValue];
    NSLog(@"offset: %f", offset);
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * offset]];
    
    gmtDate = [dateFormatter dateFromString:localTime];
    NSLog(@"GMT date: %@", gmtDate);
    
    return gmtDate;
}

- (NSString *)get24FormatTimeFrom:(NSString *)time
{
    NSString *newTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSDate *d = [dateFormatter dateFromString:time];
    newTime = [dateFormatter stringFromDate:d];
    [dateFormatter setDateFormat:@"HH:mm"];
    newTime = [dateFormatter stringFromDate:d];
    
    return newTime;
}

//- (int)getIconContextFromDate:(NSString *)date withOffset:(NSNumber *)offset withSunrise:(NSString *)sunrise withSunset:(NSString *)sunset
//{
//    // if is date (hour) between sunrise and sunset ---> return ICON_TYPE_DAILY
//    if ([[self getFormattedTimeFromString:date andOffset:offset haveToReturnDate:[NSNumber numberWithBool:YES] haveToReturnTime:[NSNumber numberWithBool:YES]] compare:sunrise] == NSOrderedDescending) {
//        if ([[self getFormattedTimeFromString:date andOffset:offset haveToReturnDate:[NSNumber numberWithBool:YES] haveToReturnTime:[NSNumber numberWithBool:YES]] compare:sunset] == NSOrderedAscending) {
//            return ICON_CONTEXT_DAILY;
//        }
//    }
//    
//    return ICON_CONTEXT_NIGHTLY;
//}

- (int)getIconContextFromDate:(NSDate *)date withSunrise:(NSDate *)sunrise withSunset:(NSDate *)sunset
{
    NSLog(@"date:    %@", date);
    NSLog(@"sunrise: %@", sunrise);
    NSLog(@"sunset:  %@", sunset);
    if ([date compare:sunrise] == NSOrderedDescending) {
        if ([date compare:sunset] == NSOrderedAscending) {
            NSLog(@"ICON_CONTEXT_DAILY");
            return ICON_CONTEXT_DAILY;
        }
    }
    NSLog(@"ICON_CONTEXT_NIGHTLY");
    return ICON_CONTEXT_NIGHTLY;
}

- (int)getCurrentIconContexFromDate:(NSDate *)date withOffset:(NSNumber *)offset withSunrise:(NSDate *)sunrise withSunset:(NSDate *)sunset
{
    NSDate *gmtDate;
//    double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSString *str = [dateFormatter stringFromDate:date];
    gmtDate = [dateFormatter dateFromString:str];
    
    if ([date compare:sunrise] == NSOrderedDescending) {
        if ([date compare:sunset] == NSOrderedAscending) {
            NSLog(@"ICON_CONTEXT_DAILY");
            return ICON_CONTEXT_DAILY;
        }
    }
    NSLog(@"ICON_CONTEXT_NIGHTLY");
    return ICON_CONTEXT_NIGHTLY;
}

- (NSArray *)getFormattedDateTimeFromDate:(NSDate *)date withOffset:(NSNumber *)offset
{
//    NSLog(@"date inside: %@", date);
    NSMutableArray *arrayOfFormats = [[NSMutableArray alloc] init];
    
    double myTimezoneOffset = [[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if ([[Util getSharedPrefs:_USES_LOCAL_TIME_VAR_NAME] boolValue] == NO) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * -([offset doubleValue] - myTimezoneOffset)]];
    } else if ([[Util getSharedPrefs:_USES_LOCAL_TIME_VAR_NAME] boolValue] == YES) {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600 * 0]];
    }
    
    NSString *timeFormat;
    if ([[Util getSharedPrefs:_HOUR_FORMAT_VAR_NAME] intValue] == HOUR_FORMAT_24) {
        if ([[Util getSharedPrefs:_USES_LONG_HOUR_FORMAT_VAR_NAME] boolValue] == YES) {
            timeFormat = @"HH:mm";
        } else {
            timeFormat = @"H:mm";
        }
    } else if ([[Util getSharedPrefs:_HOUR_FORMAT_VAR_NAME] intValue] == HOUR_FORMAT_AM_PM) {
        if ([[Util getSharedPrefs:_USES_LONG_HOUR_FORMAT_VAR_NAME] boolValue] == YES) {
            timeFormat = @"hh:mm a";
        } else {
            timeFormat = @"h:mm a";
        }
    }
    [dateFormatter setDateFormat:timeFormat];
    
    [arrayOfFormats addObject:[dateFormatter stringFromDate:date]];
    
    NSString *dateFormat;
    dateFormat = @"dd.MM.YYYY";
    //        dateFormat = @"MM/dd/YYYY";
    //        dateFormat = @"dd/MM/YYYY";
    //        dateFormat = @"YYYY/MM/dd";
    //        dateFormat = @"dd MMM.YYYY";
    //        dateFormat = @"MMM. dd,YYYY";
    //        dateFormat = @"EEE. dd MMM.";
    //        dateFormat = @"EEE., MMM. dd";
    //        dateFormat = @"MMMM dd";
    //        dateFormat = @"dd MMMM";
    //        dateFormat = @"EEEE dd MMMM";
    //        dateFormat = @"EEEE, MMMM dd";
    //        dateFormat = @"dd.MM.YY";
    //        dateFormat = @"EEE., dd.MMM.";
    
    [dateFormatter setDateFormat:dateFormat];
    
    [arrayOfFormats addObject:[dateFormatter stringFromDate:date]];
    
    NSString *weekdayFormat;
    weekdayFormat = @"EEEE";
//    weekdayFormat = @"EEE";
    
    [dateFormatter setDateFormat:weekdayFormat];
    
    [arrayOfFormats addObject:[dateFormatter stringFromDate:date]];
    
    return arrayOfFormats;
}

@end