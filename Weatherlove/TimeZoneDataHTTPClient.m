//
//  TimeZoneDataHTTPClient.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "TimeZoneDataHTTPClient.h"

static NSString * const WorldWeatherOnlineAPIKey = @"2rr249nrxq5ykc6ddhm59kyy";

static NSString * const WorldWeatherOnlineURLString = @"http://api.worldweatheronline.com/premium/v1/";

@implementation TimeZoneDataHTTPClient

+ (TimeZoneDataHTTPClient *)sharedTimeZoneDataHTTPClient
{
    static TimeZoneDataHTTPClient *_sharedTimeZoneDataHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedTimeZoneDataHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:WorldWeatherOnlineURLString]];
    });
    
    return _sharedTimeZoneDataHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)updateTimeZoneAtLocation:(CLLocation *)locationLatLng :(NSString *)locationString
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    if (locationLatLng != nil && locationString == nil) {
        parameters[@"q"] = [NSString stringWithFormat:@"%f,%f",locationLatLng.coordinate.latitude,locationLatLng.coordinate.longitude];
    } else if (locationString != nil && locationLatLng == nil) {
        parameters[@"q"] = locationString;
    }
    parameters[@"format"] = @"json";
    parameters[@"key"] = WorldWeatherOnlineAPIKey;
    
    [self GET:@"tz.ashx" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([self.delegate respondsToSelector:@selector(timeZoneDataHTTPClient:didUpdateWithTimeZone:onOriginalLocation:)]) {
            if (locationLatLng != nil && locationString == nil) {
                [self.delegate timeZoneDataHTTPClient:self didUpdateWithTimeZone:responseObject onOriginalLocation:locationLatLng];
            } else if (locationString != nil && locationLatLng == nil) {
                [self.delegate timeZoneDataHTTPClient:self didUpdateWithTimeZone:responseObject onOriginalLocation:locationString];
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(timeZoneDataHTTPClient:didFailWithError:)]) {
            [self.delegate timeZoneDataHTTPClient:self didFailWithError:error];
        }
    }];
}

@end
