//
//  TimeZoneData.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeZoneData : NSObject

@property (nonatomic, strong) NSString *timezone_query;
@property (nonatomic, strong) NSString *timezone_type;
@property (nonatomic, strong) NSString *timezone_localTime;
@property (nonatomic, strong) NSString *timezone_utcOffset;

+ (TimeZoneData *)sharedTimeZoneDataInstance;

@end
