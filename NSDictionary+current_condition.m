//
//  NSDictionary+current_condition.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+current_condition.h"

@implementation NSDictionary (current_condition)

- (NSNumber *)cloudCover_current
{
    NSString *str = self[@"cloudcover"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)feelsLikeC_current
{
    NSString *str = self[@"FeelsLikeC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)feelsLikeF_current
{
    NSString *str = self[@"FeelsLikeF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)humidity_current
{
    NSString *str = self[@"humidity"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)isDaytime_current
{
    NSString *str = self[@"isdaytime"];
    NSNumber *n = @([str boolValue]);
    return n;
}

- (NSString *)localObservationDateTime_current
{
    NSString *date = self[@"localObsDateTime"];
    return date;
}

- (NSString *)observationTime_current
{
    NSString *date = self[@"observation_time"];
    return date;
}

- (NSDate *)gmtObservationDateTime_current
{
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
//    NSString *date = self[@"localObsDateTime"];
//    NSDate *gmtDate = [timeFactory getUTCDateFromLocalDate:date];
//    
//    return gmtDate;
    
    NSDate *date = self[@"gmtObservationDateTime_current"];
    return date;
}

- (NSNumber *)precipitation_current
{
    NSString *str = self[@"precipMM"];
    NSNumber *n = @([str doubleValue]);
    return n;
}

- (NSNumber *)pressure_current
{
    NSString *str = self[@"pressure"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)tempC_current
{
    NSString *str = self[@"temp_C"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)tempF_current
{
    NSString *str = self[@"temp_F"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)visibility_current
{
    NSString *str = self[@"visibility"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)weatherCode_current
{
    NSString *str = self[@"weatherCode"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSString *)weatherDescription_current
{
    NSArray *ar = self[@"weatherDesc"];
    NSDictionary *dict = ar[0];
    return dict[@"value"];
}

- (NSURL *)weatherIconURL_current
{
    NSArray *ar = self[@"weatherIconUrl"];
    NSDictionary *dict = ar[0];
    return dict[@"value"];
}

- (NSString *)windDirection16Point_current
{
    NSString *str = self[@"winddir16Point"];
    return str;
}

- (NSNumber *)windDirectionDegree_current
{
    NSString *str = self[@"winddirDegree"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windSpeedKmph_current
{
    NSString *str = self[@"windspeedKmph"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windSpeedMiles_current
{
    NSString *str = self[@"windspeedMiles"];
    NSNumber *n = @([str intValue]);
    return n;
}

@end
