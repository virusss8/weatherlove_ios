//
//  NSDictionary+weather_forecast.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+weather_forecast.h"

@implementation NSDictionary (weather_forecast)

- (NSDictionary *)astronomy
{
    NSArray *ar = self[@"astronomy"];
    NSDictionary *dict = ar[0];
    return dict;
}

- (NSString *)dateStringOfForecast
{
    NSString *str = self[@"date"];
    return str;
}

- (NSDate *)dateOfForecast
{
    NSDate *date = self[@"dateOfForecast"];
    return date;
}

- (NSDate *)dateMoonrise
{
//    NSArray *ar = self[@"astronomy"];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
////    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], [timeFactory get24FormatTimeFrom:ar[0][@"moonrise"]]];
//    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], ar[0][@"moonrise"]];
//    NSDate *gmtDate = [timeFactory getUTCDateFromLocalDate:concateDate];
//    return gmtDate;
    
    NSDate *date = self[@"dateMoonrise"];
    return date;
}

- (NSDate *)dateMoonset
{
//    NSArray *ar = self[@"astronomy"];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
////    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], [timeFactory get24FormatTimeFrom:ar[0][@"moonset"]]];
//    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], ar[0][@"moonset"]];
//    NSDate *gmtDate = [timeFactory getUTCDateFromLocalDate:concateDate];
//    return gmtDate;
    
    NSDate *date = self[@"dateMoonset"];
    return date;
}

- (NSDate *)dateSunrise
{
//    NSArray *ar = self[@"astronomy"];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
////    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], [timeFactory get24FormatTimeFrom:ar[0][@"sunrise"]]];
//    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], ar[0][@"sunrise"]];
////    NSLog(@"sunrise_concat1 = %@", concateDate);
//    NSDate *gmtDate = [timeFactory getUTCDateFromLocalDate:concateDate];
////    NSLog(@"sunrise_concat2 = %@", concateDate);
//    return gmtDate;
    
    NSDate *date = self[@"dateSunrise"];
    return date;
}

- (NSDate *)dateSunset
{
//    NSArray *ar = self[@"astronomy"];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
////    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], [timeFactory get24FormatTimeFrom:ar[0][@"sunset"]]];
//    NSString *concateDate = [NSString stringWithFormat:@"%@ %@", self[@"date"], ar[0][@"sunset"]];
////    NSLog(@"sunset_concat1 = %@", concateDate);
//    NSDate *gmtDate = [timeFactory getUTCDateFromLocalDate:concateDate];
////    NSLog(@"sunset_concat2 = %@", concateDate);
//    return gmtDate;
    
    NSDate *date = self[@"dateSunset"];
    return date;
}

- (NSArray *)hourly
{
    NSArray *ar = self[@"hourly"];
    return ar;
}

- (NSDictionary *)daily
{
    NSArray *ar = self[@"hourly"];
    NSDictionary *dict = ar[0];
    return dict;
}

- (NSNumber *)maxTempC
{
    NSString *str = self[@"maxtempC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)maxTempF
{
    NSString *str = self[@"maxtempF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)minTempC
{
    NSString *str = self[@"mintempC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)minTempF
{
    NSString *str = self[@"mintempF"];
    NSNumber *n = @([str intValue]);
    return n;
}

@end
