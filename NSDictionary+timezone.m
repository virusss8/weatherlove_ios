//
//  NSDictionary+timezone.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 04/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+timezone.h"

@implementation NSDictionary (timezone)

- (NSString *)timezone_query
{
    NSDictionary *dict = self[@"data"];
    NSArray *ar = dict[@"request"];
    return ar[0][@"query"];
}

- (NSString *)timezone_type
{
    NSDictionary *dict = self[@"data"];
    NSArray *ar = dict[@"request"];
    return ar[0][@"type"];
}

- (NSString *)timezone_localTime
{
    NSDictionary *dict = self[@"data"];
    NSArray *ar = dict[@"time_zone"];
    return ar[0][@"localtime"];
}

- (NSNumber *)timezone_utcOffset
{
    NSDictionary *dict = self[@"data"];
    NSArray *ar = dict[@"time_zone"];
    return ar[0][@"utcOffset"];
}

- (NSDate *)timezone_utcTime
{
//    NSDictionary *dict = self[@"data"];
//    NSArray *ar = dict[@"time_zone"];
//    NSString *str = ar[0][@"localtime"];
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
//    NSDate *gmtTime = [timeFactory getUTCDateFromLocalDate:str];
//    
//    return gmtTime;
    
    NSDictionary *dict = self[@"data"];
    NSArray *ar = dict[@"time_zone"];
    return ar[0][@"timezone_utcTime"];
}

@end
