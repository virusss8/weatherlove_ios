//
//  ObservedLocation.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 24/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CurrentConditions.h"

@interface ObservedLocation : NSObject

@property (nonatomic, strong) NSNumber *offset;
@property (nonatomic, strong) NSDate *obtainedTime;
@property (nonatomic, strong) CurrentConditions *currentConditions;
@property (nonatomic, strong) NSMutableArray *dailyForecast;
@property (nonatomic, strong) NSMutableArray *hourlyForecast;

@end
