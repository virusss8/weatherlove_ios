//
//  NSDictionary+weather_forecast_astronomy.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+weather_forecast_astronomy.h"

@implementation NSDictionary (weather_forecast_astronomy)

- (NSString *)moonrise
{
    NSString *str = self[@"moonrise"];
    return str;
}

- (NSString *)moonset
{
    NSString *str = self[@"moonset"];
    return str;
}

- (NSString *)sunrise
{
    NSString *str = self[@"sunrise"];
    return str;
}

- (NSString *)sunset
{
    NSString *str = self[@"sunset"];
    return str;
}

@end
