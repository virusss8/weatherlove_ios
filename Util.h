//
//  Util.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#define SEOUL_LAT @"37.5326"
#define SEOUL_LON @"127.024612"

#define SAN_FRANCISCO_LAT @"37.775"
#define SAN_FRANCISCO_LON @"-122.4183333"

#define _CURRENT_CONDITIONS_NUM_OF_DAYS_FORECAST @5

#define _ROOT_LOCATIONS_WITH_WEATHER_DATA @"_root_array_of_locations_with_all_weather_data"
#define _WEATHER_FORECAST_HOURLY @"_weather_forecast_hourly"
#define _WEATHER_FORECAST_DAILY @"_weather_forecast_daily"
#define _WEATHER_TIMEZONE_DATA @"_timezone_data"

#define _USES_LOCAL_TIME_VAR_NAME @"_uses_local_time"

#define _DATE_FORMAT_VAR_NAME @"_date_format"

#define _HOUR_FORMAT_VAR_NAME @"_hour_format"

#define _USES_LONG_HOUR_FORMAT_VAR_NAME @"_uses_long_hour_format"

#define _METRIC_FORMAT_VAR_NAME @"_metric_format"

#define _DEGREE_FORMAT_VAR_NAME @"_degree_format"

#define _ICON_TYPE_VAR_NAME @"_icon_type"


#import <Foundation/Foundation.h>

@interface Util : NSObject

//+ (Util *)sharedUtilInstance;
+ (BOOL)setSharedPrefs:(NSString *)name withValue:(id)value;
+ (id)getSharedPrefs:(NSString *)name;
+ (BOOL)setWeatherData:(ObservedLocation *)data atIndex:(NSUInteger)index;
+ (BOOL)deleteWeatherDataAtIndex:(NSUInteger)index;
+ (ObservedLocation *)getWeatherData:(NSUInteger)locationID;
+ (void)prepareWeatherDataAtIndex:(NSUInteger)index :(NSDictionary *)timezoneData :(NSDictionary *)dailyData :(NSDictionary *)hourlyData
+ (void)toString:(ObservedLocation *)observedLocation;

typedef enum {
    ICON_CONTEXT_DAILY = 0,
    ICON_CONTEXT_NIGHTLY = 1
} TypeOfIconWithTimeContext;

typedef enum {
    HOUR_FORMAT_24 = 2,
    HOUR_FORMAT_AM_PM = 3
} TypeOfHourFormat;

typedef enum {
    METRIC_FORMAT_KM = 4,
    METRIC_FORMAT_MILE = 5
} TypeOfMetricFormat;

typedef enum {
    DEGREE_FORMAT_CELSIUS = 6,
    DEGREE_FORMAT_FAHRENHEIT = 7
} TypeOfDegreeFormat;

typedef enum {
    ICON_TYPE_TFS_ORIGINAL = 8,
    ICON_TYPE_TFS_FLATTIES = 9,
    ICON_TYPE_CLIMA_DARK = 10,
    ICON_TYPE_CLIMA_LIGHT = 11,
    ICON_TYPE_TICK = 12
} TypeOfWeatherIcons;

@end
