//
//  NSDictionary+weather_package.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 27/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (weather_package)

- (NSDictionary *)currentCondition;
- (NSDictionary *)requestData;
- (NSArray *)weatherForecast;

@end
