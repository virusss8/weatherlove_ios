//
//  NSDictionary+request.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+request.h"

@implementation NSDictionary (request)

- (NSString *)query
{
    NSString *str = self[@"query"];
    return str;
}

- (NSString *)type
{
    NSString *str = self[@"type"];
    return str;
}

@end
