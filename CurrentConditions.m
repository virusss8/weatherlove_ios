//
//  CurrentConditions.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "CurrentConditions.h"

@implementation CurrentConditions

+ (CurrentConditions *)sharedCurrentConditionsInstance
{
    static CurrentConditions *_sharedCurrentConditionsInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCurrentConditionsInstance = [[self alloc] init];
    });
    
    return _sharedCurrentConditionsInstance;
}

@end
