//
//  TimeZoneData.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "TimeZoneData.h"

@implementation TimeZoneData

+ (TimeZoneData *)sharedTimeZoneDataInstance
{
    static TimeZoneData *_sharedTimeZoneDataInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedTimeZoneDataInstance = [[self alloc] init];
    });
    
    return _sharedTimeZoneDataInstance;
}

@end