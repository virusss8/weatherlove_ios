//
//  DailyForecast.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "DailyForecast.h"

@implementation DailyForecast

+ (DailyForecast *)sharedDailyForecastInstance
{
    static DailyForecast *_sharedDailyForecastInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDailyForecastInstance = [[self alloc] init];
    });
    
    return _sharedDailyForecastInstance;
}

@end
