//
//  Util.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "Util.h"

@implementation Util

//+ (Util *)sharedUtilInstance
//{
//    static Util *_sharedUtilInstance = nil;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _sharedUtilInstance = [[self alloc] init];
//    });
//    
//    return _sharedUtilInstance;
//}

+ (BOOL)setSharedPrefs:(NSString *)name withValue:(id)value {
    @try {
        NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
        [sharedPrefs setObject:value forKey:name];
        [sharedPrefs synchronize];
        return TRUE;
    }
    @catch (NSException *exception) {
        return FALSE;
    }
}

+ (id)getSharedPrefs:(NSString *)name {
    NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
    
    id object = [sharedPrefs objectForKey:name];
    
    if (object == nil) {
        if ([name isEqualToString:_USES_LOCAL_TIME_VAR_NAME]) {
            return [NSNumber numberWithBool:NO];
            
        } else if ([name isEqualToString:_DATE_FORMAT_VAR_NAME]) {
            return [NSString stringWithFormat:@"dd.MM.yyyy"];
            
        } else if ([name isEqualToString:_HOUR_FORMAT_VAR_NAME]) {
            return [NSNumber numberWithInt:HOUR_FORMAT_24];
            
        } else if ([name isEqualToString:_USES_LONG_HOUR_FORMAT_VAR_NAME]) {
            return [NSNumber numberWithBool:YES];
            
        } else if ([name isEqualToString:_METRIC_FORMAT_VAR_NAME]) {
            return [NSNumber numberWithInt:METRIC_FORMAT_KM];
            
        } else if ([name isEqualToString:_DEGREE_FORMAT_VAR_NAME]) {
            return [NSNumber numberWithInt:DEGREE_FORMAT_CELSIUS];
            
        } else if ([name isEqualToString:_ICON_TYPE_VAR_NAME]) {
            return [NSNumber numberWithInt:ICON_TYPE_TFS_FLATTIES];
        
        } else {
         NSLog(@"sharedPrefs: object is nil");
            return nil;
        }
    } else {
        return object;
    }
}

+ (BOOL)setWeatherData:(ObservedLocation *)data atIndex:(NSUInteger)index
{
//    NSLog(@"NAME_: %@", name);
//    @try {
//        NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
//        NSMutableArray *savedLocations = [[sharedPrefs objectForKey:_ROOT_LOCATIONS_WITH_WEATHER_DATA] mutableCopy];
//        NSMutableDictionary *currentLocation = [[NSMutableDictionary alloc] init];
//        
//        if ([name isEqualToString:_WEATHER_FORECAST_DAILY]) {
//            value = [Util setAndPrepareRawDataDaily:[value mutableCopy]];
//        } else if ([name isEqualToString:_WEATHER_FORECAST_HOURLY]) {
//            value = [Util setAndPrepareRawDataHourly:[value mutableCopy]];
//        } else if ([name isEqualToString:_WEATHER_TIMEZONE_DATA]) {
//            value = [Util setAndPrepareRawDataTimezone:[value mutableCopy]];
//        } else {
//            return NO;
//        }
//        
//        if (savedLocations == nil) {
//            savedLocations = [[NSMutableArray alloc] init];
//            
//            [currentLocation setValue:value forKey:name];
//            
//            [savedLocations addObject:currentLocation];
//            
//            [sharedPrefs setObject:savedLocations forKey:_ROOT_LOCATIONS_WITH_WEATHER_DATA];
//            [sharedPrefs synchronize];
//            
//            return YES;
//        } else {
//            currentLocation = [savedLocations[0] mutableCopy];
//            
//            [currentLocation setValue:value forKey:name];
//            
//            [savedLocations replaceObjectAtIndex:0 withObject:currentLocation];
//            
//            [sharedPrefs setObject:savedLocations forKey:_ROOT_LOCATIONS_WITH_WEATHER_DATA];
//            [sharedPrefs synchronize];
//            
//            return YES;
//        }
//    }
//    @catch (NSException *exception) {
//        NSLog(@"EXCEPTION CATCHED: %@", exception);
//        return NO;
//    }
    
    @try {
        NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
        NSMutableArray *savedLocations = [[sharedPrefs objectForKey:@"_all_saved_locations"] mutableCopy];
        
        [savedLocations insertObject:data atIndex:index];
        
        [sharedPrefs setObject:savedLocations forKey:@"_all_saved_locations"];
        [sharedPrefs synchronize];
        
        return YES;
    }
    @catch (NSException *exception) {
        NSLog(@"NSException_UTIL_setWeatherData: %@", exception);
        
        return NO;
    }
}

+ (BOOL)deleteWeatherDataAtIndex:(NSUInteger)index
{
    @try {
        NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
        NSMutableArray *savedLocations = [[sharedPrefs objectForKey:@"_all_saved_locations"] mutableCopy];
        
        [savedLocations removeObjectAtIndex:index];
        
        [sharedPrefs setObject:savedLocations forKey:@"_all_saved_locations"];
        [sharedPrefs synchronize];
        
        return YES;
    }
    @catch (NSException *exception) {
        NSLog(@"NSException_UTIL_setWeatherData: %@", exception);
        
        return NO;
    }
}

+ (ObservedLocation *)getWeatherData:(NSUInteger)locationID
{
//    NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
//    
//    NSArray *savedLocations = [sharedPrefs objectForKey:_ROOT_LOCATIONS_WITH_WEATHER_DATA];
//    
//    NSDictionary* dictionary = savedLocations[0][name];
//    
//    return dictionary;
    
    @try {
        NSUserDefaults *sharedPrefs = [NSUserDefaults standardUserDefaults];
        
        __block ObservedLocation *returnObject;
        
        [[sharedPrefs objectForKey:@"_all_saved_locations"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if (idx == locationID) {
                returnObject = obj;
            }
        }];
        
        return returnObject;
    }
    @catch (NSException *exception) {
        NSLog(@"NSException_UTIL_getWeatherData: %@", exception);
        
        return nil;
    }
}

+ (NSDictionary *)setAndPrepareRawDataDaily:(NSMutableDictionary *)rawData
{
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
    NSString *date;
    NSDate *gmtDate;

    // gmt time of current condition
    date = [[rawData currentCondition] localObservationDateTime_current];
    NSLog(@"date originale: %@", date);
    gmtDate = [timeFactory getUTCDateTimeFromLocalDate:date];
//    [[rawData currentCondition] setObject:gmtDate forKey:@"gmtObservationDateTime_current"];
    
    // for daily forecast gmt time date for sunrise, sunset, moonrise, moonset
    for (int day = 0; day < [[rawData weatherForecast] count]; day++) {
        NSString *dateStringOfForecast = [[rawData weatherForecast][day] dateStringOfForecast];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] sunrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateSunrise"];
//        NSLog(@"setAndPrepare sunrise: %@", date);
//        NSLog(@"setAndPrepare sunrise: %@", [[rawData weatherForecast][day] dateSunrise]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] sunset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateSunset"];
//        NSLog(@"setAndPrepare sunset: %@", date);
//        NSLog(@"setAndPrepare sunset: %@", [[rawData weatherForecast][day] dateSunset]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] moonrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateMoonrise"];
//        NSLog(@"setAndPrepare moonrise: %@", date);
//        NSLog(@"setAndPrepare moonrise: %@", [[rawData weatherForecast][day] dateMoonrise]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] moonset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateMoonset"];
//        NSLog(@"setAndPrepare moonset: %@", date);
//        NSLog(@"setAndPrepare moonset: %@", [[rawData weatherForecast][day] dateMoonset]);
        
        [[rawData weatherForecast][day] setValue:[timeFactory getUTCDateFromUTCDateDailyWWO:[[rawData weatherForecast][day] dateStringOfForecast]] forKey:@"dateOfForecast"];
    }
    
    return rawData;
}

+ (NSDictionary *)setAndPrepareRawDataHourly:(NSMutableDictionary *)rawData
{
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
    NSString *date;
    NSDate *gmtDate;
    
    // for daily forecast gmt time date for sunrise, sunset, moonrise, moonset
    for (int day = 0; day < [[rawData weatherForecast] count]; day++) {
        
        NSString *dateStringOfForecast = [[rawData weatherForecast][day] dateStringOfForecast];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] sunrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateSunrise"];
        //        NSLog(@"setAndPrepare sunrise: %@", date);
//        NSLog(@"setAndPrepare sunrise: %@", [[rawData weatherForecast][day] dateSunrise]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] sunset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateSunset"];
        //        NSLog(@"setAndPrepare sunset: %@", date);
//        NSLog(@"setAndPrepare sunset: %@", [[rawData weatherForecast][day] dateSunset]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] moonrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateMoonrise"];
        //        NSLog(@"setAndPrepare moonrise: %@", date);
//        NSLog(@"setAndPrepare moonrise: %@", [[rawData weatherForecast][day] dateMoonrise]);
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[rawData weatherForecast][day] astronomy] moonset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [[rawData weatherForecast][day] setValue:gmtDate forKey:@"dateMoonset"];
        //        NSLog(@"setAndPrepare moonset: %@", date);
//        NSLog(@"setAndPrepare moonset: %@", [[rawData weatherForecast][day] dateMoonset]);
        
        for (int hour = 0; hour < [[[rawData weatherForecast][day] hourly] count]; hour++) {
//            NSLog(@"DAY: %i - HOUR: %i", day, hour);
            date = [NSString stringWithFormat:@"%@ %@", [[[rawData weatherForecast][day] hourly][hour] UTCDate], [timeFactory getTransformedMilitaryTimeIn24Format:[[[rawData weatherForecast][day] hourly][hour] UTCTime]]];
            gmtDate = [timeFactory getUTCDateTimeFromUTCDateHourlyWWO:date];
            [[[rawData weatherForecast][day] hourly][hour] setValue:gmtDate forKey:@"UTCDateTime"];
//            NSLog(@"hour: %@", date);
            
//            NSLog(@"sunset: %@", [[rawData weatherForecast][day] dateSunset]);
//            NSLog(@"sunrise: %@", [[rawData weatherForecast][day] dateSunrise]);
            int iconContext = [timeFactory getIconContextFromDate:gmtDate withSunrise:[[rawData weatherForecast][day] dateSunrise] withSunset:[[rawData weatherForecast][day] dateSunset]];
//            NSLog(@"icon Context: %i", iconContext);
            [[[rawData weatherForecast][day] hourly][hour] setValue:[NSNumber numberWithInt:iconContext] forKey:@"isDayTime"];
            
//            if ([[[[rawData weatherForecast][day] hourly][hour] isDayTime] intValue] == 0) {
//                NSLog(@"DAILY icon");
//            } else {
//                NSLog(@"NIGHTLY icon");
//            }
        }
    }
    
    return rawData;
}

+ (NSDictionary *)setAndPrepareRawDataTimezone:(NSMutableDictionary *)rawData
{
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
//    NSLog(@"old time: %@", [rawData timezone_localTime]);
    NSDate *gmtTime = [timeFactory getUTCDateTimeFromUTCDateTimezoneWWO:[rawData timezone_localTime]];
//    NSLog(@"new time: %@", gmtTime);
    [[rawData objectForKey:@"data"][@"time_zone"][0] setValue:gmtTime forKey:@"timezone_utcTime"];
    
//    NSLog(@"dejansko stanje: %@", [rawData timezone_utcTime]);
    
    return rawData;
}

+ (void)prepareWeatherDataAtIndex:(NSUInteger)index :(NSDictionary *)timezoneData :(NSDictionary *)dailyData :(NSDictionary *)hourlyData
{
//    NSLog(@"timezone: %@", timezoneData);
//    NSLog(@"dailydat: %@", dailyData);
//    NSLog(@"hourlyda: %@", hourlyData);
    
    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
    
    ObservedLocation *observedLocation = [[ObservedLocation alloc] init];

    [observedLocation setOffset:[timezoneData timezone_utcOffset]];
    [observedLocation setObtainedTime:[timeFactory getUTCDateTimeFromLocalDate:[[dailyData currentCondition] localObservationDateTime_current]]];
    
    CurrentConditions *cc = [[CurrentConditions alloc] init];
    
    [cc setCloudCover:[[dailyData currentCondition] cloudCover_current]];
    
    [cc setFeelsLikeC:[[dailyData currentCondition] feelsLikeC_current]];
    
    [cc setFeelsLikeF:[[dailyData currentCondition] feelsLikeF_current]];
    
    [cc setHumidity:[[dailyData currentCondition] humidity_current]];
    
    [cc setIsDaytime:[[dailyData currentCondition] isDaytime_current]];
    
    [cc setPrecipitation:[[dailyData currentCondition] precipitation_current]];
    
    [cc setPressure:[[dailyData currentCondition] pressure_current]];
    
    [cc setTempC:[[dailyData currentCondition] tempC_current]];
    
    [cc setTempF:[[dailyData currentCondition] tempF_current]];
    
    [cc setVisibility:[[dailyData currentCondition] visibility_current]];
    
    [cc setWeatherCode:[[dailyData currentCondition] weatherCode_current]];
    
    [cc setWeatherDescription:[[dailyData currentCondition] weatherDescription_current]];
    
    [cc setWindDirection16Point:[[dailyData currentCondition] windDirection16Point_current]];
    
    [cc setWindDirectionDegree:[[dailyData currentCondition] windDirectionDegree_current]];
    
    [cc setWindSpeedKmph:[[dailyData currentCondition] windSpeedKmph_current]];
    
    [cc setWindSpeedMiles:[[dailyData currentCondition] windSpeedMiles_current]];
    
    [observedLocation setCurrentConditions:cc];
    
    NSMutableArray *dailyForecastArray = [[NSMutableArray alloc] init];
    NSMutableArray *hourlyForecastInnerArray = [[NSMutableArray alloc] init];
    NSMutableArray *hourlyForecastOuterArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    DailyForecast *df = [[DailyForecast alloc] init];
    HourlyForecast *hf = [[HourlyForecast alloc] init];
    
    NSString *date;
    NSDate *gmtDate;
    
    for (int day = 0; day < [[dailyData weatherForecast] count]; day++) {
        
        [df setDateOfForecast:[timeFactory getUTCDateFromUTCDateDailyWWO:[[dailyData weatherForecast][day] dateStringOfForecast]]];

        NSString *dateStringOfForecast = [[dailyData weatherForecast][day] dateStringOfForecast];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[dailyData weatherForecast][day] astronomy] moonrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [df setTimeOfMoonrise:gmtDate];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[dailyData weatherForecast][day] astronomy] moonset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [df setTimeOfMoonset:gmtDate];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[dailyData weatherForecast][day] astronomy] sunrise]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [df setTimeOfSunrise:gmtDate];
        
        date = [NSString stringWithFormat:@"%@ %@", dateStringOfForecast, [[[dailyData weatherForecast][day] astronomy] sunset]];
        gmtDate = [timeFactory getUTCDateTimeFromUTCDate:date];
        [df setTimeOfSunset:gmtDate];

        [df setMaxTempC:[[dailyData weatherForecast][day] maxTempC]];
        
        [df setMinTempC:[[dailyData weatherForecast][day] minTempC]];
        
        [df setMaxTempF:[[dailyData weatherForecast][day] maxTempF]];
        
        [df setMinTempF:[[dailyData weatherForecast][day] minTempF]];
        
        [df setChanceOfFog:[[[dailyData weatherForecast][day] daily] chanceOfFog]];
        
        [df setChanceOfFrost:[[[dailyData weatherForecast][day] daily] chanceOfFrost]];
        
        [df setChanceOfHighTemp:[[[dailyData weatherForecast][day] daily] chanceOfHighTemp]];
        
        [df setChanceOfOvercast:[[[dailyData weatherForecast][day] daily] chanceOfOvercast]];
        
        [df setChanceOfRain:[[[dailyData weatherForecast][day] daily] chanceOfRain]];
        
        [df setChanceOfRemdry:[[[dailyData weatherForecast][day] daily] chanceOfRemdry]];
        
        [df setChanceOfSnow:[[[dailyData weatherForecast][day] daily] chanceOfSnow]];
        
        [df setChanceOfSunshine:[[[dailyData weatherForecast][day] daily] chanceOfSunshine]];
        
        [df setChanceOfThunder:[[[dailyData weatherForecast][day] daily] chanceOfThunder]];
        
        [df setChanceOfWindy:[[[dailyData weatherForecast][day] daily] chanceOfWindy]];
        
        [df setCloudCover:[[[dailyData weatherForecast][day] daily] cloudCover]];
        
        [df setDewPointC:[[[dailyData weatherForecast][day] daily] dewPointC]];
        
        [df setDewPointF:[[[dailyData weatherForecast][day] daily] dewPointF]];
        
        [df setFeelsLikeC:[[[dailyData weatherForecast][day] daily] feelsLikeC]];
        
        [df setFeelsLikeF:[[[dailyData weatherForecast][day] daily] feelsLikeF]];
        
        [df setHeatIndexC:[[[dailyData weatherForecast][day] daily] heatIndexC]];
        
        [df setHeatIndexF:[[[dailyData weatherForecast][day] daily] heatIndexF]];
        
        [df setHumidity:[[[dailyData weatherForecast][day] daily] humidity]];
        
        [df setPrecipitation:[[[dailyData weatherForecast][day] daily] precipitation]];
        
        [df setPressure:[[[dailyData weatherForecast][day] daily] pressure]];
        
        [df setTempC:[[[dailyData weatherForecast][day] daily] tempC]];
        
        [df setTempF:[[[dailyData weatherForecast][day] daily] tempF]];
        
        [df setVisibility:[[[dailyData weatherForecast][day] daily] visibility]];
        
        [df setWeatherCode:[[[dailyData weatherForecast][day] daily] weatherCode]];
        
        [df setWeatherDescription:[[[dailyData weatherForecast][day] daily] weatherDescription]];
        
        [df setWindChillC:[[[dailyData weatherForecast][day] daily] windChillC]];
        
        [df setWindChillF:[[[dailyData weatherForecast][day] daily] windChillF]];
        
        [df setWindDirection16Point:[[[dailyData weatherForecast][day] daily] windDirection16Point]];
        
        [df setWindDirectionDegree:[[[dailyData weatherForecast][day] daily] windDirectionDegree]];
        
        [df setWindGustKmph:[[[dailyData weatherForecast][day] daily] windGustKmph]];
        
        [df setWindGustMiles:[[[dailyData weatherForecast][day] daily] windGustMiles]];
        
        [df setWindSpeedKmph:[[[dailyData weatherForecast][day] daily] windSpeedKmph]];
        
        [df setWindSpeedMiles:[[[dailyData weatherForecast][day] daily] windSpeedMiles]];
        
        [dailyForecastArray addObject:df];
        
        df = [DailyForecast new];
        
        [dict setObject:[timeFactory getUTCDateFromUTCDateDailyWWO:[[dailyData weatherForecast][day] dateStringOfForecast]] forKey:@"dateOfHourlyForecast"];
        
        for (int hour = 0; hour < [[[hourlyData weatherForecast][day] hourly] count]; hour++) {
            
            [hf setChanceOfFog:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfFog]];
            
            [hf setChanceOfFrost:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfFrost]];
            
            [hf setChanceOfHighTemp:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfHighTemp]];
            
            [hf setChanceOfOvercast:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfOvercast]];
            
            [hf setChanceOfRain:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfRain]];
            
            [hf setChanceOfRemdry:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfRemdry]];
            
            [hf setChanceOfSnow:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfSnow]];
            
            [hf setChanceOfSunshine:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfSunshine]];
            
            [hf setChanceOfThunder:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfThunder]];
            
            [hf setChanceOfWindy:[[[hourlyData weatherForecast][day] hourly][hour] chanceOfWindy]];
            
            [hf setCloudCover:[[[hourlyData weatherForecast][day] hourly][hour] cloudCover]];
            
            [hf setDewPointC:[[[hourlyData weatherForecast][day] hourly][hour] dewPointC]];
            
            [hf setDewPointF:[[[hourlyData weatherForecast][day] hourly][hour] dewPointF]];
            
            [hf setFeelsLikeC:[[[hourlyData weatherForecast][day] hourly][hour] feelsLikeC]];
            
            [hf setFeelsLikeF:[[[hourlyData weatherForecast][day] hourly][hour] feelsLikeF]];
            
            [hf setHeatIndexC:[[[hourlyData weatherForecast][day] hourly][hour] heatIndexC]];
            
            [hf setHeatIndexF:[[[hourlyData weatherForecast][day] hourly][hour] heatIndexF]];
            
            [hf setHumidity:[[[hourlyData weatherForecast][day] hourly][hour] humidity]];
            
            [hf setIsDaytime:[[[hourlyData weatherForecast][day] hourly][hour] isDaytime]];
            
            [hf setPrecipitation:[[[hourlyData weatherForecast][day] hourly][hour] precipitation]];
            
            [hf setPressure:[[[hourlyData weatherForecast][day] hourly][hour] pressure]];
            
            [hf setTempC:[[[hourlyData weatherForecast][day] hourly][hour] tempC]];
            
            [hf setTempF:[[[hourlyData weatherForecast][day] hourly][hour] tempF]];
            
            date = [NSString stringWithFormat:@"%@ %@", [[[hourlyData weatherForecast][day] hourly][hour] UTCDate], [timeFactory getTransformedMilitaryTimeIn24Format:[[[hourlyData weatherForecast][day] hourly][hour] UTCTime]]];
            gmtDate = [timeFactory getUTCDateTimeFromUTCDateHourlyWWO:date];
            [hf setUTCDateTime:gmtDate];
            
            [hf setVisibility:[[[hourlyData weatherForecast][day] hourly][hour] visibility]];
            
            [hf setWeatherCode:[[[hourlyData weatherForecast][day] hourly][hour] weatherCode]];
        
            [hf setWeatherDescription:[[[hourlyData weatherForecast][day] hourly][hour] weatherDescription]];
            
            [hf setWindChillC:[[[hourlyData weatherForecast][day] hourly][hour] windChillC]];
            
            [hf setWindChillF:[[[hourlyData weatherForecast][day] hourly][hour] windChillF]];
            
            [hf setWindDirection16Point:[[[hourlyData weatherForecast][day] hourly][hour] windDirection16Point]];
            
            [hf setWindDirectionDegree:[[[hourlyData weatherForecast][day] hourly][hour] windDirectionDegree]];
            
            [hf setWindGustKmph:[[[hourlyData weatherForecast][day] hourly][hour] windGustKmph]];
            
            [hf setWindGustMiles:[[[hourlyData weatherForecast][day] hourly][hour] windGustMiles]];
            
            [hf setWindSpeedKmph:[[[hourlyData weatherForecast][day] hourly][hour] windSpeedKmph]];
            
            [hf setWindSpeedMiles:[[[hourlyData weatherForecast][day] hourly][hour] windSpeedMiles]];
            
            [hourlyForecastInnerArray addObject:hf];
            
            hf = [HourlyForecast new];
        }
        
        [dict setObject:hourlyForecastInnerArray forKey:@"hourlyData"];
        
        hourlyForecastInnerArray = [NSMutableArray new];
        
//        NSLog(@"dict 01: %@", dict);
        
        [hourlyForecastOuterArray addObject:dict];
        
        dict = [NSMutableDictionary new];
        
//        NSLog(@"dict 02: %@", dict);
    }
    
    [observedLocation setDailyForecast:dailyForecastArray];
    [observedLocation setHourlyForecast:hourlyForecastOuterArray];
    
//    [Util toString:observedLocation];
    
    [Util setWeatherData:observedLocation atIndex:index];
}

+ (void)toString:(ObservedLocation *)observedLocation
{
    NSLog(@"offset: %@", [observedLocation offset]);
    NSLog(@"obtained_time: %@", [observedLocation obtainedTime]);
    
    NSLog(@"cc cloudCover_current: %@", [observedLocation.currentConditions cloudCover]);
    NSLog(@"cc feelsLikeC_current: %@", [observedLocation.currentConditions feelsLikeC]);
    NSLog(@"cc feelsLikeF_current: %@", [observedLocation.currentConditions feelsLikeF]);
    NSLog(@"cc humidity_current: %@", [observedLocation.currentConditions humidity]);
    NSLog(@"cc isDaytime_current: %@", [observedLocation.currentConditions isDaytime]);
    NSLog(@"cc precipitation_current: %@", [observedLocation.currentConditions precipitation]);
    NSLog(@"cc pressure_current: %@", [observedLocation.currentConditions pressure]);
    NSLog(@"cc tempC_current: %@", [observedLocation.currentConditions tempC]);
    NSLog(@"cc tempF_current: %@", [observedLocation.currentConditions tempF]);
    NSLog(@"cc visibility_current: %@", [observedLocation.currentConditions visibility]);
    NSLog(@"cc weatherCode_current: %@", [observedLocation.currentConditions weatherCode]);
    NSLog(@"cc weatherDescription_current: %@", [observedLocation.currentConditions weatherDescription]);
    NSLog(@"cc windDirection16Point_current: %@", [observedLocation.currentConditions windDirection16Point]);
    NSLog(@"cc windDirectionDegree_current: %@", [observedLocation.currentConditions windDirectionDegree]);
    NSLog(@"cc windSpeedKmph_current: %@", [observedLocation.currentConditions windSpeedKmph]);
    NSLog(@"cc windSpeedMiles_current: %@", [observedLocation.currentConditions windSpeedMiles]);
    NSLog(@"_______________________________________________");
    
    for (int day = 0; day < [[observedLocation dailyForecast] count]; day++) {
        NSLog(@"dateOfForecast: %@", [[observedLocation dailyForecast][day] dateOfForecast]);
        
        NSLog(@"timeOfMoonrise: %@", [[observedLocation dailyForecast][day] timeOfMoonrise]);
        NSLog(@"timeOfMoonset: %@", [[observedLocation dailyForecast][day] timeOfMoonset]);
        NSLog(@"timeOfSunrise: %@", [[observedLocation dailyForecast][day] timeOfSunrise]);
        NSLog(@"timeOfSunset: %@", [[observedLocation dailyForecast][day] timeOfSunset]);
        
        NSLog(@"maxTempC: %@", [[observedLocation dailyForecast][day] maxTempC]);
        NSLog(@"minTempC: %@", [[observedLocation dailyForecast][day] minTempC]);
        NSLog(@"maxTempF: %@", [[observedLocation dailyForecast][day] maxTempF]);
        NSLog(@"minTempF: %@", [[observedLocation dailyForecast][day] minTempF]);
        NSLog(@"chanceOfFog: %@", [[observedLocation dailyForecast][day] chanceOfFog]);
        NSLog(@"chanceOfFrost: %@", [[observedLocation dailyForecast][day] chanceOfFrost]);
        NSLog(@"chanceOfHighTemp: %@", [[observedLocation dailyForecast][day] chanceOfHighTemp]);
        NSLog(@"chanceOfOvercast: %@", [[observedLocation dailyForecast][day] chanceOfOvercast]);
        NSLog(@"chanceOfRain: %@", [[observedLocation dailyForecast][day] chanceOfRain]);
        NSLog(@"chanceOfRemdry: %@", [[observedLocation dailyForecast][day] chanceOfRemdry]);
        NSLog(@"chanceOfSnow: %@", [[observedLocation dailyForecast][day] chanceOfSnow]);
        NSLog(@"chanceOfSunshine: %@", [[observedLocation dailyForecast][day] chanceOfSunshine]);
        NSLog(@"chanceOfThunder: %@", [[observedLocation dailyForecast][day] chanceOfThunder]);
        NSLog(@"chanceOfWindy: %@", [[observedLocation dailyForecast][day] chanceOfWindy]);
        NSLog(@"cloudCover: %@", [[observedLocation dailyForecast][day] cloudCover]);
        NSLog(@"dewPointC: %@", [[observedLocation dailyForecast][day] dewPointC]);
        NSLog(@"dewPointF: %@", [[observedLocation dailyForecast][day] dewPointF]);
        NSLog(@"feelsLikeC: %@", [[observedLocation dailyForecast][day] feelsLikeC]);
        NSLog(@"feelsLikeF: %@", [[observedLocation dailyForecast][day] feelsLikeF]);
        NSLog(@"heatIndexC: %@", [[observedLocation dailyForecast][day] heatIndexC]);
        NSLog(@"heatIndexF: %@", [[observedLocation dailyForecast][day] heatIndexF]);
        NSLog(@"humidity: %@", [[observedLocation dailyForecast][day] humidity]);
        NSLog(@"precipitation: %@", [[observedLocation dailyForecast][day] precipitation]);
        NSLog(@"pressure: %@", [[observedLocation dailyForecast][day] pressure]);
        NSLog(@"tempC: %@", [[observedLocation dailyForecast][day] tempC]);
        NSLog(@"tempF: %@", [[observedLocation dailyForecast][day] tempF]);
        NSLog(@"visibility: %@", [[observedLocation dailyForecast][day] visibility]);
        NSLog(@"weatherCode: %@", [[observedLocation dailyForecast][day] weatherCode]);
        NSLog(@"weatherDescription: %@", [[observedLocation dailyForecast][day] weatherDescription]);
        NSLog(@"windChillC: %@", [[observedLocation dailyForecast][day] windChillC]);
        NSLog(@"windChillF: %@", [[observedLocation dailyForecast][day] windChillF]);
        NSLog(@"windDirection16Point: %@", [[observedLocation dailyForecast][day] windDirection16Point]);
        NSLog(@"windDirectionDegree: %@", [[observedLocation dailyForecast][day] windDirectionDegree]);
        NSLog(@"windGustKmph: %@", [[observedLocation dailyForecast][day] windGustKmph]);
        NSLog(@"windGustMiles: %@", [[observedLocation dailyForecast][day] windGustMiles]);
        NSLog(@"windSpeedKmph: %@", [[observedLocation dailyForecast][day] windSpeedKmph]);
        NSLog(@"windSpeedMiles: %@", [[observedLocation dailyForecast][day] windSpeedMiles]);
        
        for (int hour = 0; hour < [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"] count]; hour++) {
            //            NSLog(@"hour %i", hour);
            NSLog(@"cfog: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfFog]);
            NSLog(@"cfro: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfFrost]);
            NSLog(@"chig: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfHighTemp]);
            NSLog(@"cove: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfOvercast]);
            NSLog(@"crai: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfRain]);
            NSLog(@"crem: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfRemdry]);
            NSLog(@"csno: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfSnow]);
            NSLog(@"csun: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfSunshine]);
            NSLog(@"cthu: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfThunder]);
            NSLog(@"cwin: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] chanceOfWindy]);
            NSLog(@"ccov: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] cloudCover]);
            NSLog(@"dewc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] dewPointC]);
            NSLog(@"dewf: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] dewPointF]);
            NSLog(@"felc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] feelsLikeC]);
            NSLog(@"felf: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] feelsLikeF]);
            NSLog(@"hinc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] heatIndexC]);
            NSLog(@"hinf: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] heatIndexF]);
            NSLog(@"humi: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] humidity]);
            NSLog(@"isdt: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] isDaytime]);
            NSLog(@"prec: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] precipitation]);
            NSLog(@"pres: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] pressure]);
            NSLog(@"temc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] tempC]);
            NSLog(@"temf: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] tempF]);
            NSLog(@"utcd: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] UTCDateTime]);
            NSLog(@"visi: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] visibility]);
            NSLog(@"wcod: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] weatherCode]);
            NSLog(@"desc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] weatherDescription]);
            NSLog(@"wchc: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windChillC]);
            NSLog(@"wchf: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windChillF]);
            NSLog(@"wd16: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windDirection16Point]);
            NSLog(@"wdde: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windDirectionDegree]);
            NSLog(@"wgkm: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windGustKmph]);
            NSLog(@"wgmi: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windGustMiles]);
            NSLog(@"wskm: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windSpeedKmph]);
            NSLog(@"wsmi: %@", [[[observedLocation hourlyForecast][day] objectForKey:@"hourlyData"][hour] windSpeedMiles]);
            
            NSLog(@"__________");
        }
    }
}

@end
