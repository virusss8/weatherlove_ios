//
//  DailyForecast.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DailyForecast : NSObject

@property (nonatomic, strong) NSNumber *chanceOfFog;
@property (nonatomic, strong) NSNumber *chanceOfFrost;
@property (nonatomic, strong) NSNumber *chanceOfHighTemp;
@property (nonatomic, strong) NSNumber *chanceOfOvercast;
@property (nonatomic, strong) NSNumber *chanceOfRain;
@property (nonatomic, strong) NSNumber *chanceOfRemdry;
@property (nonatomic, strong) NSNumber *chanceOfSnow;
@property (nonatomic, strong) NSNumber *chanceOfSunshine;
@property (nonatomic, strong) NSNumber *chanceOfThunder;
@property (nonatomic, strong) NSNumber *chanceOfWindy;
@property (nonatomic, strong) NSNumber *cloudCover;
@property (nonatomic, strong) NSNumber *dewPointC;
@property (nonatomic, strong) NSNumber *dewPointF;
@property (nonatomic, strong) NSNumber *feelsLikeC;
@property (nonatomic, strong) NSNumber *feelsLikeF;
@property (nonatomic, strong) NSNumber *heatIndexC;
@property (nonatomic, strong) NSNumber *heatIndexF;
@property (nonatomic, strong) NSNumber *humidity;
@property (nonatomic, strong) NSNumber *precipitation; // double
@property (nonatomic, strong) NSNumber *pressure;
@property (nonatomic, strong) NSNumber *tempC;
@property (nonatomic, strong) NSNumber *tempF;
//@property (nonatomic, strong) NSNumber *time_hourly; // 24 means it is daily forecast
@property (nonatomic, strong) NSNumber *visibility;
@property (nonatomic, strong) NSNumber *weatherCode;
@property (nonatomic, strong) NSString *weatherDescription;
@property (nonatomic, strong) NSNumber *windChillC;
@property (nonatomic, strong) NSNumber *windChillF;
@property (nonatomic, strong) NSString *windDirection16Point;
@property (nonatomic, strong) NSNumber *windDirectionDegree;
@property (nonatomic, strong) NSNumber *windGustKmph;
@property (nonatomic, strong) NSNumber *windGustMiles;
@property (nonatomic, strong) NSNumber *windSpeedKmph;
@property (nonatomic, strong) NSNumber *windSpeedMiles;

@property (nonatomic, strong) NSNumber *maxTempC;
@property (nonatomic, strong) NSNumber *maxTempF;
@property (nonatomic, strong) NSNumber *minTempC;
@property (nonatomic, strong) NSNumber *minTempF;
@property (nonatomic, strong) NSDate *dateOfForecast;
@property (nonatomic, strong) NSDate *timeOfMoonrise;
@property (nonatomic, strong) NSDate *timeOfMoonset;
@property (nonatomic, strong) NSDate *timeOfSunrise;
@property (nonatomic, strong) NSDate *timeOfSunset;

//@property (nonatomic, strong) NSString *weekday; // day of the week
//@property (nonatomic, strong) NSString *date; // date in selected format (sre, mar. 5 etc.)
//@property (nonatomic, strong) UIImage *icon; // weather icon
//@property (nonatomic, strong) NSNumber *minTemperature; // minTempC or minTempF
//@property (nonatomic, strong) NSNumber *maxTemperature; // maxTempC or maxTempF
//@property (nonatomic, strong) NSNumber *windSpeed; // windSpeedKMH or windSpeedMILES

+ (DailyForecast *)sharedDailyForecastInstance;

@end
