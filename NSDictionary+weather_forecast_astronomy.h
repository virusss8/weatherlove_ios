//
//  NSDictionary+weather_forecast_astronomy.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (weather_forecast_astronomy)

- (NSString *)moonrise;
- (NSString *)moonset;
- (NSString *)sunrise;
- (NSString *)sunset;

@end
