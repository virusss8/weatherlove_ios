//
//  NSDictionary+weather_forecast_hourly.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "NSDictionary+weather_forecast_hourly.h"

@implementation NSDictionary (weather_forecast_hourly)

- (NSNumber *)chanceOfFog
{
    NSString *str = self[@"chanceoffog"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfFrost
{
    NSString *str = self[@"chanceoffrost"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfHighTemp
{
    NSString *str = self[@"chanceofhightemp"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfOvercast
{
    NSString *str = self[@"chanceofovercast"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfRain
{
    NSString *str = self[@"chanceofrain"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfRemdry
{
    NSString *str = self[@"chanceofremdry"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfSnow
{
    NSString *str = self[@"chanceofsnow"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfSunshine
{
    NSString *str = self[@"chanceofsunshine"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfThunder
{
    NSString *str = self[@"chanceofthunder"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)chanceOfWindy
{
    NSString *str = self[@"chanceofwindy"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)cloudCover
{
    NSString *str = self[@"cloudcover"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)dewPointC
{
    NSString *str = self[@"DewPointC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)dewPointF
{
    NSString *str = self[@"DewPointF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)feelsLikeC
{
    NSString *str = self[@"FeelsLikeC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)feelsLikeF
{
    NSString *str = self[@"FeelsLikeF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)heatIndexC
{
    NSString *str = self[@"HeatIndexC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)heatIndexF
{
    NSString *str = self[@"HeatIndexF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)humidity
{
    NSString *str = self[@"humidity"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)isDaytime
{
    NSString *str = self[@"isdaytime"];
    NSNumber *n = @([str boolValue]);
    return n;
}

- (NSNumber *)precipitation
{
    NSString *str = self[@"precipMM"];
    NSNumber *n = @([str doubleValue]);
    return n;
}

- (NSNumber *)pressure
{
    NSString *str = self[@"pressure"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)tempC
{
    NSString *str = self[@"tempC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)tempF
{
    NSString *str = self[@"tempF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSString *)time_hourly
{
    NSString *str = self[@"time"];
    return str;
}

- (NSString *)UTCDate
{
    NSString *str = self[@"UTCdate"];
    return str;
}

- (NSString *)UTCTime
{
    NSString *str = self[@"UTCtime"];
//    NSNumber *n = @([str intValue]);
    return str;
}

- (NSDate *)UTCDateTime
{
//    TimeFactory *timeFactory = [TimeFactory sharedTimeFactoryInstance];
//    NSString *str = [NSString stringWithFormat:@"%@ %@", self[@"UTCdate"], [timeFactory getTransformedMilitaryTimeIn24Format:self[@"time"]]];
//    NSDate *gmtTime = [timeFactory getUTCDateFromLocalDate:str];
//    
//    return gmtTime;
    
    NSDate *date = self[@"UTCDateTime"];
    return date;
}

- (NSNumber *)isDayTime
{
    NSNumber *num = self[@"isDayTime"];
    return num;
}

- (NSNumber *)visibility
{
    NSString *str = self[@"visibility"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)weatherCode
{
    NSString *str = self[@"weatherCode"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSString *)weatherDescription
{
    NSArray *ar = self[@"weatherDesc"];
    NSDictionary *dict = ar[0];
    return dict[@"value"];
}

- (NSURL *)weatherIconURL
{
    NSArray *ar = self[@"weatherIconUrl"];
    NSDictionary *dict = ar[0];
    return dict[@"value"];
}

- (NSNumber *)windChillC
{
    NSString *str = self[@"WindChillC"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windChillF
{
    NSString *str = self[@"WindChillF"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSString *)windDirection16Point
{
    NSString *str = self[@"winddir16Point"];
    return str;
}

- (NSNumber *)windDirectionDegree
{
    NSString *str = self[@"winddirDegree"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windGustKmph
{
    NSString *str = self[@"WindGustKmph"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windGustMiles
{
    NSString *str = self[@"WindGustMiles"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windSpeedKmph
{
    NSString *str = self[@"windspeedKmph"];
    NSNumber *n = @([str intValue]);
    return n;
}

- (NSNumber *)windSpeedMiles
{
    NSString *str = self[@"windspeedMiles"];
    NSNumber *n = @([str intValue]);
    return n;
}

@end
