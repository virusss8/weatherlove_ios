//
//  HourlyForecast.m
//  Weatherlove
//
//  Created by Tadej Prasnikar on 06/03/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import "HourlyForecast.h"

@implementation HourlyForecast

+ (HourlyForecast *)sharedHourlyForecastInstance
{
    static HourlyForecast *_sharedHourlyForecastInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHourlyForecastInstance = [[self alloc] init];
    });
    
    return _sharedHourlyForecastInstance;
}

@end
