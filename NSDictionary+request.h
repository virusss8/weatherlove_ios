//
//  NSDictionary+request.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (request)

- (NSString *)query; // string
- (NSString *)type; // string

@end
