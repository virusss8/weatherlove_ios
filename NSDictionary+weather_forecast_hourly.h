//
//  NSDictionary+weather_forecast_hourly.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeFactory.h"

@interface NSDictionary (weather_forecast_hourly)

- (NSNumber *)chanceOfFog;
- (NSNumber *)chanceOfFrost;
- (NSNumber *)chanceOfHighTemp;
- (NSNumber *)chanceOfOvercast;
- (NSNumber *)chanceOfRain;
- (NSNumber *)chanceOfRemdry;
- (NSNumber *)chanceOfSnow;
- (NSNumber *)chanceOfSunshine;
- (NSNumber *)chanceOfThunder;
- (NSNumber *)chanceOfWindy;
- (NSNumber *)cloudCover;
- (NSNumber *)dewPointC;
- (NSNumber *)dewPointF;
- (NSNumber *)feelsLikeC;
- (NSNumber *)feelsLikeF;
- (NSNumber *)heatIndexC;
- (NSNumber *)heatIndexF;
- (NSNumber *)humidity;
- (NSNumber *)isDaytime; // boolean - HOURLY ONLY
- (NSNumber *)precipitation; // double
- (NSNumber *)pressure;
- (NSNumber *)tempC;
- (NSNumber *)tempF;
- (NSString *)time_hourly; // 24 means it is daily forecast, 100(300, 700..) military time for hourly forecast
- (NSString *)UTCDate; // YYYY-MM-DD - HOURLY ONLY
- (NSString *)UTCTime; // time in UTC = 0 (militart format also) - HOURLY ONLY
/*!!!*/- (NSDate *)UTCDateTime;
- (NSNumber *)isDayTime;
- (NSNumber *)visibility;
- (NSNumber *)weatherCode;
- (NSString *)weatherDescription;
- (NSURL *)weatherIconURL;
- (NSNumber *)windChillC;
- (NSNumber *)windChillF;
- (NSString *)windDirection16Point;
- (NSNumber *)windDirectionDegree;
- (NSNumber *)windGustKmph;
- (NSNumber *)windGustMiles;
- (NSNumber *)windSpeedKmph;
- (NSNumber *)windSpeedMiles;

@end
