//
//  NSDictionary+weather_forecast.h
//  Weatherlove
//
//  Created by Tadej Prasnikar on 28/02/2014.
//  Copyright (c) 2014 TFStoritve. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (weather_forecast)

- (NSDictionary *)astronomy;
- (NSString *)dateStringOfForecast;
/*!!!*/- (NSDate *)dateOfForecast;
/*!!!*/- (NSDate *)dateMoonrise;
/*!!!*/- (NSDate *)dateMoonset;
/*!!!*/- (NSDate *)dateSunrise;
/*!!!*/- (NSDate *)dateSunset;
- (NSArray *)hourly;
- (NSDictionary *)daily;
- (NSNumber *)maxTempC;
- (NSNumber *)maxTempF;
- (NSNumber *)minTempC;
- (NSNumber *)minTempF;

@end
